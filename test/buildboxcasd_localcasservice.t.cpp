/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_casserver.h>
#include <buildboxcasd_localcasservice.h>
#include <buildboxcasd_proxymodefixture.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_temporarydirectory.h>

#include <gmock/gmock.h>
#include <grpcpp/client_context.h>
#include <grpcpp/impl/codegen/call.h>
#include <grpcpp/server_context.h>
#include <grpcpp/support/sync_stream.h>
#include <gtest/gtest.h>

#include <fstream>
#include <iostream>
#include <memory.h>
#include <stdlib.h>
#include <unistd.h>

using namespace buildboxcasd;
using namespace buildboxcommon;

void generateAndStoreFiles(std::map<std::string, std::string> *file_contents,
                           std::map<std::string, Digest> *file_digests,
                           std::shared_ptr<LocalCas> storage)
{
    (*file_contents)["file1.sh"] = "file1Contents...";
    (*file_digests)["file1.sh"] = make_digest((*file_contents)["file1.sh"]);

    storage->writeBlob((*file_digests)["file1.sh"],
                       (*file_contents)["file1.sh"]);

    (*file_contents)["file2.c"] = "file2: [...data...]";
    (*file_digests)["file2.c"] = make_digest((*file_contents)["file2.c"]);

    storage->writeBlob((*file_digests)["file2.c"],
                       (*file_contents)["file2.c"]);
}

static Digest
createDirectoryTree(const std::map<std::string, Digest> &file_digests,
                    std::shared_ptr<LocalCas> storage)
{

    /*
     *  root/
     *  |-- file1.sh*
     *  |-- link -> subdir1/file2.c
     *  |-- subdir1/
     *           |-- file2.c
     */

    // Creating `root/`:
    Directory root_directory;

    // Adding a file to it:
    FileNode *f1 = root_directory.add_files();
    f1->set_name("file1.sh");
    f1->set_is_executable(true);
    f1->mutable_digest()->CopyFrom(file_digests.at("file1.sh"));

    // Adding symlink (`link` -> `subdir1/file2.c`)
    SymlinkNode *link = root_directory.add_symlinks();
    link->set_name("link");
    link->set_target("subdir1/file2.c");

    // Creating `subdir1/`:
    Directory subdirectory;

    // Adding `subdir1/file2.c`:
    FileNode *f2 = subdirectory.add_files();
    f2->set_name("file2.c");
    f2->mutable_digest()->CopyFrom(file_digests.at("file2.c"));

    // Adding `subdir1/` under `dirA/`:
    DirectoryNode *d1 = root_directory.add_directories();
    d1->set_name("subdir1");

    const auto serialized_subdirectory = subdirectory.SerializeAsString();
    const auto subdirectory_digest = make_digest(serialized_subdirectory);
    d1->mutable_digest()->CopyFrom(subdirectory_digest);

    const auto serialized_root_directory = root_directory.SerializeAsString();
    const auto root_directory_digest = make_digest(serialized_root_directory);

    // Storing the two Directory protos...
    storage->writeBlob(root_directory_digest, serialized_root_directory);
    storage->writeBlob(subdirectory_digest, serialized_subdirectory);

    return root_directory_digest;
}

static bool fileContentMatches(const std::string &file_path,
                               const std::string &data)
{
    std::ifstream file(file_path, std::ios::binary);
    std::stringstream file_content;
    file_content << file.rdbuf();

    return data == file_content.str();
}

class RemoteServerFixture : public CasProxyModeFixture {
  protected:
    RemoteServerFixture() : CasProxyModeFixture() {}

    void SetUp() override
    {
        generateAndStoreFiles(&file_contents, &file_digests, local_storage);
        root_directory_digest =
            createDirectoryTree(file_digests, local_storage);
    }

    template <class T>
    void getProtoFromLocalStorage(const Digest &digest, T &t)
    {
        const auto type_blob = local_storage->readBlob(digest);
        t.ParseFromString(type_blob);
    }

    Digest root_directory_digest;

    std::map<std::string, Digest> file_digests;
    std::map<std::string, std::string> file_contents;
};

/*
 * Write content to a random file, in directory temp_dir and return the
 * filename or the full path to the file.
 */
std::string write_file_to_existing_dir(const std::string temp_dir,
                                       const std::string &content,
                                       bool return_full_path = false)
{
    std::string file_path = temp_dir + "/tempXXXXXX";
    // Create a temp file
    int test_file = mkstemp(&file_path[0]);

    // get the new filename created from mkstemp
    const std::string file_name = file_path.substr(file_path.rfind("/") + 1);

    // write content to file
    write(test_file, content.c_str(), sizeof(char) * content.size());
    close(test_file);

    return return_full_path ? file_path : file_name;
}

TEST_F(RemoteServerFixture, SimpleTreeCapture)
{
    CaptureTreeRequest request;
    CaptureTreeResponse response;

    // create temp dir.
    buildboxcommon::TemporaryDirectory temp_dir1;

    // create temp file in directory
    std::string content_string = "I'm buildboxcasd!";
    std::string file_name =
        write_file_to_existing_dir(string(temp_dir1.name()), content_string);

    // add directory to request "path".
    request.add_path(string(temp_dir1.name()));
    request.set_bypass_local_cache(false);

    // send request
    localcas_stub->CaptureTree(&client_context, request, &response);

    // check results match local FS structure
    ASSERT_EQ(response.responses().size(), request.path().size());

    for (const auto &r : response.responses()) {
        ASSERT_EQ(r.status().code(), grpc::StatusCode::OK);
        ASSERT_EQ(r.path(), string(temp_dir1.name()));
        // Check d_local_storage for blob/contents.
        // Get digest from tree message.
        Tree t;
        getProtoFromLocalStorage(r.tree_digest(), t);
        auto files = t.root().files();
        ASSERT_EQ(files.size(), 1);
        auto file = t.root().files(0);
        ASSERT_EQ(file.name(), file_name);
        ASSERT_EQ(local_storage->readBlob(file.digest()), content_string);
    }
}

TEST_F(RemoteServerFixture, NestedDirectoryTreeCapture)
{
    CaptureTreeRequest request;
    CaptureTreeResponse response;

    /* Directory Structure
     * /tempxxxx
     *     /temp1
     *          file1
     *      /temp2
     *          file2
     * /tempxxxx
     *     /temp3
     *          file3
     */

    const buildboxcommon::TemporaryDirectory temp_dir1;
    const std::string temp_dir2 = "temp2";
    const std::string temp_dir2_path =
        string(temp_dir1.name()) + "/" + temp_dir2;
    const buildboxcommon::TemporaryDirectory temp_dir3;

    // create directory inside root temporary directory
    FileUtils::create_directory(temp_dir2_path.c_str());

    const std::vector<std::string> content_vec = {
        "I'm buildboxcasd in temp1!", "I'm buildboxcasd in temp3!"};

    const std::vector<std::string> path_vec = {string(temp_dir1.name()),
                                               string(temp_dir3.name())};

    const std::vector<std::string> file_vec = {
        write_file_to_existing_dir(path_vec[0], content_vec[0]),
        write_file_to_existing_dir(path_vec[1], content_vec[1])};

    // specify temp2 content separately
    const std::string content_in_file2 = "I'm buildboxcasd in temp2!";
    const std::string file_in_temp_dir2 =
        write_file_to_existing_dir(temp_dir2_path, content_in_file2);

    request.add_path(path_vec[0]);
    request.add_path(path_vec[1]);

    // send request
    localcas_stub->CaptureTree(&client_context, request, &response);

    ASSERT_EQ(response.responses().size(), request.path().size());

    // Check results match local FS structure
    for (int i = 0; i < (int)path_vec.size(); ++i) {
        auto r = response.responses(i);
        ASSERT_EQ(r.status().code(), grpc::StatusCode::OK);
        ASSERT_EQ(r.path(), path_vec[i]);
        Tree t;
        getProtoFromLocalStorage(r.tree_digest(), t);
        auto files = t.root().files();
        // check subdirectory of root
        if (t.root().directories().size() != 0) {
            auto dirnode = t.root().directories(0);
            ASSERT_EQ(dirnode.name(), temp_dir2);
            Directory d;
            getProtoFromLocalStorage(dirnode.digest(), d);
            ASSERT_EQ(files.size(), 1);
            auto file = d.files(0);
            ASSERT_EQ(file.name(), file_in_temp_dir2);
            ASSERT_EQ(local_storage->readBlob(file.digest()),
                      content_in_file2);
        }
        ASSERT_EQ(files.size(), 1);
        auto file = t.root().files(0);
        ASSERT_EQ(file.name(), file_vec[i]);
        ASSERT_EQ(local_storage->readBlob(file.digest()), content_vec[i]);
    }
}

TEST_F(RemoteServerFixture, SimpleFileCapture)
{
    CaptureFilesRequest request;
    CaptureFilesResponse response;

    const buildboxcommon::TemporaryDirectory temp_dir1;
    const int size = 10;
    std::string content_string = "I'm buildboxcasd!";
    std::vector<std::string> content_vec;

    for (int i = 0; i < size; ++i) {
        auto content = content_string + std::to_string(i);
        auto file_path = write_file_to_existing_dir(string(temp_dir1.name()),
                                                    content, true);
        content_vec.push_back(content);
        request.add_path(file_path);
    }

    localcas_stub->CaptureFiles(&client_context, request, &response);

    ASSERT_EQ(response.responses().size(), size);

    for (int i = 0; i < size; ++i) {
        auto r = response.responses(i);
        ASSERT_EQ(r.status().code(), grpc::StatusCode::OK);
        ASSERT_EQ(local_storage->readBlob(r.digest()), content_vec[i]);
    }
}

TEST_F(RemoteServerFixture, RemoteCasUpload)
{

    CaptureTreeRequest request;
    CaptureTreeResponse response;

    // send simple TreeCapture
    // create temp dir.
    buildboxcommon::TemporaryDirectory temp_dir1;
    // create temp file in directory
    std::string content_string = "I'm buildboxcasd!";
    std::string file_name =
        write_file_to_existing_dir(string(temp_dir1.name()), content_string);
    request.add_path(string(temp_dir1.name()));
    request.set_bypass_local_cache(false);

    // send request
    localcas_stub->CaptureTree(&client_context, request, &response);

    // check results match local FS structure
    ASSERT_EQ(response.responses().size(), request.path().size());

    auto resp = response.responses(0);
    ASSERT_EQ(resp.status().code(), grpc::StatusCode::OK);
    ASSERT_EQ(resp.path(), string(temp_dir1.name()));

    // fetch from remote CAS
    std::string tree_message = cas_client->fetchString(resp.tree_digest());

    // fetch from local CAS
    Tree t;
    const auto type_blob = local_storage->readBlob(resp.tree_digest());
    t.ParseFromString(type_blob);

    // check that it equals the one stored locally
    ASSERT_EQ(tree_message, t.SerializeAsString());
}

TEST_F(RemoteServerFixture, StageRootDigestNotInCAS)
{
    StageTreeRequest request;
    StageTreeResponse response;

    Digest d(make_digest(""));
    d.set_hash("hash1234");
    request.mutable_root_digest()->CopyFrom(d);

    auto reader_writer = localcas_stub->StageTree(&client_context);
    ASSERT_TRUE(reader_writer->Write(request));

    ASSERT_FALSE(reader_writer->Read(&response));

    const auto status = reader_writer->Finish();
    ASSERT_EQ(status.error_code(), grpc::StatusCode::FAILED_PRECONDITION);
}

TEST_F(RemoteServerFixture, Stage)
{
    // Preparing a (valid) request:
    StageTreeRequest request;
    request.mutable_root_digest()->CopyFrom(root_directory_digest);
    ASSERT_TRUE(local_storage->hasBlob(root_directory_digest));

    // Stage():
    auto reader_writer = localcas_stub->StageTree(&client_context);
    ASSERT_TRUE(reader_writer->Write(request));

    // The tree was staged and we got a valid path in the response:
    StageTreeResponse response;
    ASSERT_TRUE(reader_writer->Read(&response));
    ASSERT_TRUE(FileUtils::is_directory(response.path().c_str()));

    // The contents that were staged match the tree we requested:
    const std::string file1_stage_path = response.path() + "/file1.sh";
    ASSERT_TRUE(FileUtils::is_regular_file(file1_stage_path.c_str()));
    ASSERT_TRUE(FileUtils::is_executable(file1_stage_path.c_str()));
    ASSERT_TRUE(
        fileContentMatches(file1_stage_path, file_contents["file1.sh"]));

    const std::string file2_stage_path = response.path() + "/subdir1/file2.c";
    ASSERT_TRUE(FileUtils::is_regular_file(file2_stage_path.c_str()));
    ASSERT_TRUE(
        fileContentMatches(file2_stage_path, file_contents["file2.c"]));

    const std::string link_stage_path = response.path() + "/link";
    ASSERT_TRUE(FileUtils::is_regular_file(link_stage_path.c_str()));
    ASSERT_TRUE(fileContentMatches(link_stage_path, file_contents["file2.c"]));

    // Ask the server to clean up:
    ASSERT_TRUE(reader_writer->Write(StageTreeRequest()));

    // Receive its last empty reply:
    ASSERT_TRUE(reader_writer->Read(&response));
    ASSERT_TRUE(response.path().empty());
    const auto status = reader_writer->Finish();
    ASSERT_EQ(status.error_code(), grpc::StatusCode::OK);

    ASSERT_FALSE(FileUtils::is_directory(response.path().c_str()));
}

TEST_F(RemoteServerFixture, StreamCloseCleansDirectory)
{
    StageTreeRequest request;
    StageTreeResponse response;

    request.mutable_root_digest()->CopyFrom(root_directory_digest);

    auto reader_writer = localcas_stub->StageTree(&client_context);
    ASSERT_TRUE(reader_writer->Write(request));
    ASSERT_TRUE(reader_writer->Read(&response));
    ASSERT_TRUE(FileUtils::is_directory(response.path().c_str()));

    reader_writer->WritesDone();
    const auto status = reader_writer->Finish();
    ASSERT_EQ(status.error_code(), grpc::StatusCode::OK);
    ASSERT_FALSE(FileUtils::is_directory(request.path().c_str()));
}

TEST_F(RemoteServerFixture, GetLocalDiskUsage)
{
    /* This is the disk usage of the content generated by the fixture
     * (2 files and 2 directories).
     */
    const int64_t fixture_disk_usage = 306;

    const std::string extra_content = std::string(42, 'x');

    {
        /* Check initial disk usage */
        grpc::ClientContext client_context;
        GetLocalDiskUsageRequest request;
        GetLocalDiskUsageResponse response;
        localcas_stub->GetLocalDiskUsage(&client_context, request, &response);
        ASSERT_EQ(response.size_bytes(), fixture_disk_usage);
        ASSERT_EQ(response.quota_bytes(), 0);
    }

    {
        /* Add a file */
        grpc::ClientContext client_context;
        CaptureFilesRequest request;
        CaptureFilesResponse response;
        const buildboxcommon::TemporaryDirectory temp_dir1;
        auto file_path = write_file_to_existing_dir(string(temp_dir1.name()),
                                                    extra_content, true);
        request.add_path(file_path);
        localcas_stub->CaptureFiles(&client_context, request, &response);
    }

    {
        /* Check disk usage again */
        grpc::ClientContext client_context;
        GetLocalDiskUsageRequest request;
        GetLocalDiskUsageResponse response;
        localcas_stub->GetLocalDiskUsage(&client_context, request, &response);
        ASSERT_EQ(response.size_bytes(),
                  fixture_disk_usage + extra_content.size());
        ASSERT_EQ(response.quota_bytes(), 0);
    }
}

class CasProxyFixture : public CasProxyModeFixture {
  protected:
    CasProxyFixture() {}

    Digest uploadTree(std::shared_ptr<LocalCas> storage)
    {
        generateAndStoreFiles(&file_contents, &file_digests, storage);
        return createDirectoryTree(file_digests, storage);
    }

    /* Sends a stage request for the given digest and verifies that the
     * contents that are made available match the files we uploaded
     */
    void stageAndAssertContentsMatch(const Digest &root_directory_digest)
    {
        // Preparing a (valid) request:
        StageTreeRequest request;
        request.mutable_root_digest()->CopyFrom(root_directory_digest);

        // Stage:
        auto reader_writer = localcas_stub->StageTree(&client_context);
        ASSERT_TRUE(reader_writer->Write(request));

        // The tree was staged and we got a valid path in the response:
        StageTreeResponse response;
        ASSERT_TRUE(reader_writer->Read(&response));
        ASSERT_TRUE(FileUtils::is_directory(response.path().c_str()));

        assertStagedDirectoryContentsMatch(response.path());

        // Ask the server to clean up:
        ASSERT_TRUE(reader_writer->Write(StageTreeRequest()));

        // Receive its last empty reply:
        ASSERT_TRUE(reader_writer->Read(&response));
        ASSERT_TRUE(response.path().empty());
        const auto status = reader_writer->Finish();
        ASSERT_EQ(status.error_code(), grpc::StatusCode::OK);

        ASSERT_FALSE(FileUtils::is_directory(response.path().c_str()));
    }

    void assertStagedDirectoryContentsMatch(const std::string &stage_root_path)
    {
        // The contents that were staged match the tree we requested:
        const std::string file1_stage_path = stage_root_path + "/file1.sh";
        ASSERT_TRUE(FileUtils::is_regular_file(file1_stage_path.c_str()));
        ASSERT_TRUE(FileUtils::is_executable(file1_stage_path.c_str()));
        ASSERT_TRUE(
            fileContentMatches(file1_stage_path, file_contents["file1.sh"]));

        const std::string file2_stage_path =
            stage_root_path + "/subdir1/file2.c";
        ASSERT_TRUE(FileUtils::is_regular_file(file2_stage_path.c_str()));
        ASSERT_TRUE(
            fileContentMatches(file2_stage_path, file_contents["file2.c"]));

        const std::string link_stage_path = stage_root_path + "/link";
        ASSERT_TRUE(FileUtils::is_regular_file(link_stage_path.c_str()));
        ASSERT_TRUE(
            fileContentMatches(link_stage_path, file_contents["file2.c"]));
    }

    std::map<std::string, Digest> file_digests;
    std::map<std::string, std::string> file_contents;
};

TEST_F(CasProxyFixture, StageRootDigestNotInCAS)
{
    StageTreeRequest request;
    StageTreeResponse response;

    Digest d(make_digest(""));
    d.set_hash("hash1234");
    request.mutable_root_digest()->CopyFrom(d);

    auto reader_writer = localcas_stub->StageTree(&client_context);
    ASSERT_TRUE(reader_writer->Write(request));

    ASSERT_FALSE(reader_writer->Read(&response));

    const auto status = reader_writer->Finish();
    ASSERT_EQ(status.error_code(), grpc::StatusCode::FAILED_PRECONDITION);
}

TEST_F(CasProxyFixture, StageWithBlobsLocally)
{
    const Digest root_directory_digest = uploadTree(local_storage);

    // The tree is available locally:
    ASSERT_TRUE(local_storage->hasBlob(root_directory_digest));
    ASSERT_FALSE(remote_storage->hasBlob(root_directory_digest));

    stageAndAssertContentsMatch(root_directory_digest);
}

TEST_F(CasProxyFixture, StageWithBlobsInRemote)
{
    const Digest root_directory_digest = uploadTree(remote_storage);

    // The tree is not available locally, but the proxy fetches it from the
    // remote before staging:
    ASSERT_FALSE(local_storage->hasBlob(root_directory_digest));
    ASSERT_TRUE(remote_storage->hasBlob(root_directory_digest));

    stageAndAssertContentsMatch(root_directory_digest);
}
