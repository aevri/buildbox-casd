/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_TESTSERVER_H
#define INCLUDED_BUILDBOXCASD_TESTSERVER_H

#include <buildboxcasd_casserver.h>
#include <buildboxcasd_localcasservice.h>
#include <gmock/gmock.h>
#include <gtest/gtest.h>

#include <buildboxcasd_localcas.h>
#include <buildboxcommon_temporarydirectory.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/impl/codegen/call.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/support/sync_stream.h>

using namespace buildboxcasd;
using namespace buildboxcommon;

class CasProxyModeFixture : public ::testing::Test {
    /**
     * This fixture can be used to test the functionality of buildbox-casd in
     * proxy mode.
     *
     * It sets up two instances: one running as a proxy and another as a
     * regular server ("remote"). Both instances use sockets for receiving
     * connections.
     *
     *                  (`cas_client`)
     * [ Proxy Server ] --------------> [ Remote Server ]
     *        |                                 |
     * (`local_storage`)                (`remote_storage`)
     *
     */
  protected:
    CasProxyModeFixture()
        : TEST_PROXY_SERVER_ADDRESS("unix://" +
                                    std::string(sockets_directory.name()) +
                                    "/proxy.sock"),
          TEST_REMOTE_SERVER_ADDRESS("unix://" +
                                     std::string(sockets_directory.name()) +
                                     "/remote.sock"),
          local_storage(std::make_shared<FsLocalCas>(
              local_storage_root_directory.name())),
          cas_client(std::make_shared<buildboxcommon::Client>()),
          local_cas_service(
              std::make_shared<CasService>(local_storage, cas_client)),
          remote_storage(std::make_shared<FsLocalCas>(
              remote_storage_root_directory.name())),
          remote_cas_service(std::make_shared<CasService>(remote_storage)),
          proxy_cas_channel(grpc::CreateChannel(
              TEST_PROXY_SERVER_ADDRESS, grpc::InsecureChannelCredentials())),
          cas_stub(ContentAddressableStorage::NewStub(proxy_cas_channel)),
          cas_bytestream_stub(ByteStream::NewStub(proxy_cas_channel)),
          localcas_stub(
              LocalContentAddressableStorage::NewStub(proxy_cas_channel))
    {
        cas_servicer = std::dynamic_pointer_cast<CasRemoteExecutionServicer>(
            local_cas_service->remoteExecutionCasServicer());

        // Building and starting the remote server:
        grpc::ServerBuilder remote_server_builder;
        remote_server_builder.AddListeningPort(
            TEST_REMOTE_SERVER_ADDRESS, grpc::InsecureServerCredentials());
        remote_server_builder.RegisterService(
            remote_cas_service->remoteExecutionCasServicer().get());
        remote_server_builder.RegisterService(
            remote_cas_service->bytestreamServicer().get());

        remote_server = remote_server_builder.BuildAndStart();

        // Building and starting the proxy server:
        grpc::ServerBuilder proxy_builder;
        proxy_builder.AddListeningPort(TEST_PROXY_SERVER_ADDRESS,
                                       grpc::InsecureServerCredentials());
        proxy_builder.RegisterService(
            local_cas_service->remoteExecutionCasServicer().get());
        proxy_builder.RegisterService(
            local_cas_service->bytestreamServicer().get());
        proxy_builder.RegisterService(
            local_cas_service->localCasServicer().get());

        proxy_server = proxy_builder.BuildAndStart();

        // Setting up the CAS Client that connects the proxy with the remote
        // server:
        buildboxcommon::ConnectionOptions connection_options;
        connection_options.d_url = TEST_REMOTE_SERVER_ADDRESS.c_str();
        cas_client->init(connection_options);
    }

    ~CasProxyModeFixture(){};

    inline static Digest make_digest(const std::string &data)
    {
        return buildboxcommon::CASHash::hash(data);
    }

    // Directory to contain the sockets where the services will listen:
    buildboxcommon::TemporaryDirectory sockets_directory;
    const std::string TEST_PROXY_SERVER_ADDRESS, TEST_REMOTE_SERVER_ADDRESS;

    // Local proxy:
    buildboxcommon::TemporaryDirectory local_storage_root_directory;
    std::shared_ptr<FsLocalCas> local_storage;
    std::shared_ptr<buildboxcommon::Client> cas_client;
    std::shared_ptr<CasService> local_cas_service;
    std::shared_ptr<CasRemoteExecutionServicer> cas_servicer;
    std::unique_ptr<grpc::Server> proxy_server;

    // Remote CAS server:
    buildboxcommon::TemporaryDirectory remote_storage_root_directory;
    std::shared_ptr<FsLocalCas> remote_storage;
    std::shared_ptr<CasService> remote_cas_service;
    std::unique_ptr<grpc::Server> remote_server;

    // Contexts:
    grpc::ClientContext client_context;
    grpc::ServerContext server_context;

    // gRPC stubs (allow to invoke gRPC calls):
    std::shared_ptr<grpc::Channel> proxy_cas_channel;
    std::unique_ptr<ContentAddressableStorage::Stub> cas_stub;
    std::unique_ptr<ByteStream::Stub> cas_bytestream_stub;
    std::unique_ptr<LocalContentAddressableStorage::Stub> localcas_stub;
};

#endif
