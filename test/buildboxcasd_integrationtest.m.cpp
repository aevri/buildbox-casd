/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_integrationtest.m.h>

#include <buildboxcommon_fileutils.h>
#include <gtest/gtest.h>
#include <iostream>

std::string CASD_PROXY_ADDRESS, CASD_SERVER_ADDRESS;
std::string INPUT_DIRECTORY, OUTPUT_DIRECTORY;

/* This binary will receive two arguments pointing to two CASd instances,
 * one running in proxy mode and the other in regular CAS server mode.
 */

void printUsage(char *program_name)
{
    std::cout << "Usage: " << program_name
              << " CASD_PROXY_ADDRESS CASD_SERVER_ADDRESS INPUT_DIRECTORY "
                 "OUTPUT_DIRECTORY"
              << std::endl;
}

int main(int argc, char *argv[])
{
    ::testing::InitGoogleTest(&argc, argv);

    if (argc < 5) {
        printUsage(argv[0]);
        return -1;
    }

    CASD_PROXY_ADDRESS = std::string(argv[1]);
    CASD_SERVER_ADDRESS = std::string(argv[2]);

    INPUT_DIRECTORY = std::string(argv[3]);
    OUTPUT_DIRECTORY = std::string(argv[4]);

    if (!buildboxcommon::FileUtils::is_directory(INPUT_DIRECTORY.c_str())) {
        std::cerr << "Invalid INPUT_DIRECTORY: " << INPUT_DIRECTORY
                  << std::endl;
        return -1;
    }

    if (!buildboxcommon::FileUtils::is_directory(OUTPUT_DIRECTORY.c_str())) {
        std::cerr << "Invalid OUTPUT_DIRECTORY: " << OUTPUT_DIRECTORY
                  << std::endl;
        return -1;
    }

    if (!buildboxcommon::FileUtils::directory_is_empty(
            OUTPUT_DIRECTORY.c_str())) {
        std::cerr << "OUTPUT_DIRECTORY must be empty" << std::endl;
        return -1;
    }

    return RUN_ALL_TESTS();
}
