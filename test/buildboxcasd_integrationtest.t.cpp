/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <gtest/gtest.h>

#include <buildboxcommon_cashash.h>
#include <buildboxcommon_client.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_protos.h>
#include <buildboxcommon_temporarydirectory.h>

#include <buildboxcasd_integrationtest.m.h>

using buildboxcommon::Digest;

inline static Digest make_digest(const std::string &data)
{
    return buildboxcommon::CASHash::hash(data);
}

class IntegrationTestFixture : public ::testing::Test {
  protected:
    IntegrationTestFixture()
        : input_path(INPUT_DIRECTORY), output_path(OUTPUT_DIRECTORY)
    {
        buildboxcommon::ConnectionOptions proxy_options;
        proxy_options.d_url = CASD_PROXY_ADDRESS.c_str();
        proxy_client.init(proxy_options);

        buildboxcommon::ConnectionOptions server_options;
        server_options.d_url = CASD_SERVER_ADDRESS.c_str();
        server_client.init(server_options);
    }

    buildboxcommon::Client proxy_client;
    buildboxcommon::Client server_client;

    const std::string input_path;
    const std::string output_path;

    const std::string simple_data = "Hello World!";
    const Digest simple_data_digest = make_digest(simple_data);
};

TEST_F(IntegrationTestFixture, SimpleDataTest)
{
    const std::vector<Digest> digests = {simple_data_digest};
    const std::vector<Digest> missing_digests =
        proxy_client.findMissingBlobs(digests);

    ASSERT_EQ(missing_digests.size(), 1);
    ASSERT_EQ(missing_digests.front(), digests.front());

    proxy_client.upload(simple_data, simple_data_digest);

    ASSERT_TRUE(proxy_client.findMissingBlobs({simple_data_digest}).empty());

    ASSERT_EQ(proxy_client.fetchString(simple_data_digest), simple_data);
    ASSERT_EQ(server_client.fetchString(simple_data_digest), simple_data);
}

TEST_F(IntegrationTestFixture, UploadAndDownload)
{
    Digest directory_digest;
    ASSERT_NO_THROW(
        proxy_client.uploadDirectory(input_path, &directory_digest));

    const std::string proxy_output_path = output_path + "/proxy-output";
    buildboxcommon::FileUtils::create_directory(proxy_output_path.c_str());

    const std::string server_output_path = output_path + "/server-output";
    buildboxcommon::FileUtils::create_directory(server_output_path.c_str());

    ASSERT_NO_THROW(
        proxy_client.downloadDirectory(directory_digest, proxy_output_path));

    ASSERT_NO_THROW(
        server_client.downloadDirectory(directory_digest, server_output_path));

    // Both directories are to be `diff`ed against the input one.

    ASSERT_TRUE(
        proxy_client.uploadDirectory(input_path, &directory_digest).empty());
}
