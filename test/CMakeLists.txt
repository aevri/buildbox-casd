set(CMAKE_CXX_STANDARD 14)
include_directories(.)
include_directories(../buildboxcasd)

set(GTEST_SOURCE_ROOT CACHE FILEPATH "Path to Google Test source checkout (if unset, CMake will search for a binary copy)")

if(GTEST_SOURCE_ROOT)
    # Build GTest from the provided source directory
    add_subdirectory(${GTEST_SOURCE_ROOT} ${CMAKE_CURRENT_BINARY_DIR}/googletest)
    set(GTEST_TARGET gtest)
    set(GMOCK_TARGET gmock)
else()
    # Attempt to locate GTest using the CMake config files introduced in 1.8.1
    find_package(GTest CONFIG)
    if (GTest_FOUND)
        set(GTEST_TARGET GTest::gtest)
        set(GMOCK_TARGET GTest::gmock)
    else()
        # Attempt to locate GTest using CMake's FindGTest module
        find_package(GTest MODULE REQUIRED)

        # CMake's FindGTest module doesn't include easy support for locating GMock
        # (see https://gitlab.kitware.com/cmake/cmake/issues/17365),
        # so we hack it in by replacing "gtest" with "gmock" in the GTest configuration
        # it found. This works because GMock is included in recent versions of GTest.

        string(REPLACE "gtest" "gmock" GMOCK_LIBRARY ${GTEST_LIBRARY})
        string(REPLACE "gtest" "gmock" GMOCK_INCLUDE_DIRS ${GTEST_INCLUDE_DIRS})

        add_library(GMock::GMock UNKNOWN IMPORTED)
        set_target_properties(GMock::GMock PROPERTIES
            IMPORTED_LOCATION ${GMOCK_LIBRARY}
            INTERFACE_INCLUDE_DIRECTORIES "${GMOCK_INCLUDE_DIRS}"
            INTERFACE_LINK_LIBRARIES GTest::GTest
        )

        set(GTEST_TARGET GTest::GTest)
        set(GMOCK_TARGET GMock::GMock)
    endif()
endif()


add_executable(cas_server_tests
               buildboxcasd_casserver.t.cpp
               buildboxcasd_tests.m.cpp)
target_link_libraries(cas_server_tests ${GMOCK_TARGET} casd)

add_executable(cas_proxy_tests
               buildboxcasd_casproxy.t.cpp
               buildboxcasd_tests.m.cpp)
target_link_libraries(cas_proxy_tests ${GMOCK_TARGET} casd)

add_executable(local_cas_tests
               buildboxcasd_localcas.t.cpp
               buildboxcasd_tests.m.cpp)
target_link_libraries(local_cas_tests ${GMOCK_TARGET} casd)

add_executable(lru_local_cas_tests
               buildboxcasd_lrulocalcas.t.cpp
               buildboxcasd_tests.m.cpp)
target_link_libraries(lru_local_cas_tests ${GMOCK_TARGET} casd)

add_executable(local_cas_service_tests
               buildboxcasd_localcasservice.t.cpp
               buildboxcasd_tests.m.cpp)
target_link_libraries(local_cas_service_tests ${GMOCK_TARGET} casd)

add_executable(file_stager_tests
               buildboxcasd_filestager.t.cpp
               buildboxcasd_tests.m.cpp)
target_link_libraries(file_stager_tests ${GMOCK_TARGET} casd)


add_test(NAME cas_server_tests COMMAND cas_server_tests)
add_test(NAME local_cas_tests COMMAND local_cas_tests)
add_test(NAME lru_local_cas_tests COMMAND lru_local_cas_tests)
add_test(NAME cas_proxy_tests COMMAND cas_proxy_tests)
add_test(NAME local_cas_service_tests COMMAND local_cas_service_tests)
add_test(NAME file_stager_tests COMMAND file_stager_tests)

add_executable(cli_stager
               buildboxcasd_clistager.m.cpp)
add_dependencies(cli_stager buildbox-casd)
target_link_libraries(cli_stager casd)

add_executable(integration_test
               buildboxcasd_integrationtest.m.cpp
               buildboxcasd_integrationtest.t.cpp)
add_dependencies(integration_test buildbox-casd)
target_link_libraries(integration_test ${GMOCK_TARGET} casd)
