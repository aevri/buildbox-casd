/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_casserver.h>

#include <thread>

#include <fcntl.h>
#include <gmock/gmock.h>

#include <grpcpp/client_context.h>
#include <grpcpp/create_channel.h>
#include <grpcpp/impl/codegen/call.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>
#include <grpcpp/support/sync_stream.h>

#include <gtest/gtest.h>

#include <buildboxcommon_temporarydirectory.h>

#include <buildboxcasd_casinstance.h>
#include <buildboxcasd_proxymodefixture.h>

using namespace buildboxcasd;

class CasProxyFixture : public CasProxyModeFixture {
  protected:
    CasProxyFixture()
    {
        // Pre-loading some content in the local and remote storages:
        local_storage->writeBlob(make_digest("data1"), "data1");
        local_storage->writeBlob(make_digest("data2"), "data2");

        remote_storage->writeBlob(make_digest("remotedata3"), "remotedata3");
        remote_storage->writeBlob(make_digest("remotedata4"), "remotedata4");
    }
};

TEST_F(CasProxyFixture, FindMissingBlobs)
{
    FindMissingBlobsRequest request;

    const auto local_digest = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(local_digest));

    const auto remote_digest = make_digest("remotedata4");
    ASSERT_TRUE(remote_storage->hasBlob(remote_digest));

    const auto missing_digest = make_digest("non-existent");
    ASSERT_FALSE(local_storage->hasBlob(missing_digest));
    ASSERT_FALSE(remote_storage->hasBlob(missing_digest));

    const auto r1 = request.add_blob_digests();
    r1->CopyFrom(local_digest);
    const auto r2 = request.add_blob_digests();
    r2->CopyFrom(remote_digest);
    const auto r3 = request.add_blob_digests();
    r3->CopyFrom(missing_digest);

    FindMissingBlobsResponse response;
    cas_servicer->FindMissingBlobs(&server_context, &request, &response);

    ASSERT_EQ(response.missing_blob_digests().size(), 1);
    ASSERT_EQ(response.missing_blob_digests(0).size_bytes(),
              missing_digest.size_bytes());
    ASSERT_EQ(response.missing_blob_digests(0).hash(), missing_digest.hash());

    // The blob missing in the remote but available locally was implicitly
    // uploaded:
    ASSERT_TRUE(remote_storage->hasBlob(local_digest));
}

TEST_F(CasProxyFixture, BatchUpdateBlobs)
{
    BatchUpdateBlobsRequest request;

    const auto data4 = "data4";
    const auto data5 = "data5";

    const Digest d4 = make_digest(data4);
    const Digest d5 = make_digest(data5);

    auto entry1 = request.add_requests();
    entry1->mutable_digest()->CopyFrom(d4);
    entry1->set_data(data4);

    auto entry2 = request.add_requests();
    entry2->mutable_digest()->CopyFrom(d5);
    entry2->set_data(data5);

    BatchUpdateBlobsResponse response;
    cas_servicer->BatchUpdateBlobs(&server_context, &request, &response);

    // Response is valid:
    ASSERT_EQ(response.responses().size(), 2);

    std::set<std::string> digests;
    for (const auto &r : response.responses()) {
        ASSERT_EQ(r.status().code(), grpc::StatusCode::OK);
        digests.insert(r.digest().hash());
    }

    ASSERT_EQ(digests.count(d4.hash()), 1);
    ASSERT_EQ(digests.count(d5.hash()), 1);

    // And data was stored:
    ASSERT_TRUE(local_storage->hasBlob(d4));
    ASSERT_TRUE(local_storage->hasBlob(d5));

    ASSERT_TRUE(remote_storage->hasBlob(d4));
    ASSERT_TRUE(remote_storage->hasBlob(d5));
}

TEST_F(CasProxyFixture, BatchReadBlobs)
{
    // We'll request two blobs that we know are in the CAS:
    const auto digest1 = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(digest1));

    const auto digest2 = make_digest("remotedata3");
    ASSERT_FALSE(local_storage->hasBlob(digest2));
    ASSERT_TRUE(remote_storage->hasBlob(digest2));

    std::set<std::string> request_hashes = {digest1.hash(), digest2.hash()};

    BatchReadBlobsRequest request;
    request.add_digests()->CopyFrom(digest1);
    request.add_digests()->CopyFrom(digest2);
    ASSERT_EQ(request.digests_size(), 2);

    BatchReadBlobsResponse response;
    cas_servicer->BatchReadBlobs(&server_context, &request, &response);
    ASSERT_EQ(response.responses().size(), 2);

    for (const auto &r : response.responses()) {
        ASSERT_EQ(r.status().code(), grpc::StatusCode::OK);
        // The hash is what we asked for:
        ASSERT_EQ(request_hashes.count(r.digest().hash()), 1);
        // The data corresponds to that blob in its corresponding storage:
        if (r.digest().hash() == digest1.hash()) {
            ASSERT_EQ(r.data(), local_storage->readBlob(r.digest()));
        }
        else {
            ASSERT_EQ(r.data(), remote_storage->readBlob(r.digest()));
        }
    }

    ASSERT_TRUE(local_storage->hasBlob(digest2));
}

TEST_F(CasProxyFixture, BatchReadBlobsSizedExceeded)
{

    // Trying to fetch a blob that exceeds the gRPC message limit returns an
    // invalid-argument error:
    const std::string big_blob(
        buildboxcommon::Client::bytestreamChunkSizeBytes() + 1, 'X');
    auto digest = make_digest(big_blob);

    BatchReadBlobsRequest request;
    request.add_digests()->CopyFrom(digest);

    BatchReadBlobsResponse response;
    const grpc::Status return_status =
        cas_servicer->BatchReadBlobs(&server_context, &request, &response);
    ASSERT_EQ(return_status.error_code(), grpc::StatusCode::INVALID_ARGUMENT);
}

TEST_F(CasProxyFixture, BatchReadBlobsWithMissing)
{
    const auto digest1 = make_digest("data1");
    const auto digest2 = make_digest("dataX");

    ASSERT_TRUE(local_storage->hasBlob(digest1));
    ASSERT_FALSE(local_storage->hasBlob(digest2));
    ASSERT_FALSE(remote_storage->hasBlob(digest2));

    BatchReadBlobsRequest request;
    request.add_digests()->CopyFrom(digest1);
    request.add_digests()->CopyFrom(digest2);

    BatchReadBlobsResponse response;
    cas_servicer->BatchReadBlobs(&server_context, &request, &response);

    ASSERT_EQ(response.responses().size(), request.digests_size());

    for (const auto &r : response.responses()) {
        if (r.digest().hash() == digest1.hash()) {
            ASSERT_EQ(r.status().code(), grpc::StatusCode::OK);
            ASSERT_EQ(r.data(), local_storage->readBlob(r.digest()));
        }
        else if (r.digest().hash() == digest2.hash()) {
            ASSERT_EQ(r.status().code(), grpc::StatusCode::NOT_FOUND);
        }
        else {
            FAIL() << "Digest in response was not requested.";
        }
    }
}

TEST_F(CasProxyFixture, GetTreeWithMissingRoot)
{
    Digest root_digest;
    root_digest.set_hash("this-digest-is-not-in-the-CAS");
    ASSERT_FALSE(local_storage->hasBlob(root_digest));
    ASSERT_FALSE(remote_storage->hasBlob(root_digest));

    GetTreeRequest request;
    request.mutable_root_digest()->CopyFrom(root_digest);

    auto reader = cas_stub->GetTree(&client_context, request);

    GetTreeResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::NOT_FOUND);
}

TEST_F(CasProxyFixture, GetTree)
{
    /* We'll test this directory structure:
     *  root/
     *  |-- file1.sh*
     *  |-- subdir1/
     *           |-- file2.c
     */

    // Creating `root/`:
    Directory root_directory;

    // Adding a file to it:
    FileNode *f1 = root_directory.add_files();
    f1->set_name("file1.sh");
    f1->set_is_executable(true);
    f1->mutable_digest()->CopyFrom(make_digest("file1Contents"));

    // Creating `subdir1/`:
    Directory subdirectory;

    // Adding `subdir1/file2.c`:
    FileNode *f3 = subdirectory.add_files();
    f3->set_name("file2.c");
    f3->mutable_digest()->CopyFrom(make_digest("file3Contents"));

    // Adding `subdir1/` under `dirA/`:
    DirectoryNode *d1 = root_directory.add_directories();
    d1->set_name("subdir1");

    const auto serialized_subdirectory = subdirectory.SerializeAsString();
    const auto subdirectory_digest = make_digest(serialized_subdirectory);
    d1->mutable_digest()->CopyFrom(subdirectory_digest);

    const auto serialized_root_directory = root_directory.SerializeAsString();
    const auto root_directory_digest = make_digest(serialized_root_directory);

    // Storing the two Directory protos in the remote server:
    remote_storage->writeBlob(root_directory_digest,
                              serialized_root_directory);
    remote_storage->writeBlob(subdirectory_digest, serialized_subdirectory);

    // ... and requesting them using GetTree():
    GetTreeRequest request;
    request.mutable_root_digest()->CopyFrom(root_directory_digest);

    auto reader = cas_stub->GetTree(&client_context, request);

    // We received a valid reply:
    GetTreeResponse response;
    ASSERT_TRUE(reader->Read(&response));

    // And it contains what we want:
    ASSERT_EQ(response.directories_size(), 2); // `root/` and `root/subdir1`

    for (const auto &node : response.directories()) {
        if (node.directories_size() > 0) {
            // This node is `root/`:
            ASSERT_EQ(node.directories_size(), 1);
            ASSERT_EQ(node.files_size(), 1);
            ASSERT_EQ(node.files(0).name(), "file1.sh");
            ASSERT_TRUE(node.files(0).is_executable());

            // So `root/subdir1` is there:
            const auto subdir1 = node.directories(0);
            ASSERT_EQ(subdir1.name(), "subdir1");
            ASSERT_EQ(subdir1.digest().hash(), subdirectory_digest.hash());
        }
        else {
            // This node represents `subdir1/`:
            ASSERT_EQ(node.directories_size(), 0);
            ASSERT_EQ(node.files_size(), 1);
            ASSERT_EQ(node.files(0).name(), "file2.c");
            ASSERT_FALSE(node.files(0).is_executable());
        }
    }

    ASSERT_TRUE(local_storage->hasBlob(root_directory_digest));
    ASSERT_TRUE(local_storage->hasBlob(subdirectory_digest));
}

TEST_F(CasProxyFixture, GetPartialTree)
{
    /* We'll test this directory structure:
     *   root/
     *  |-- file1.sh*
     *  |-- subdir1/
     *           |-- file2.c
     *
     * But the server won't have `subdir1/` in its CAS.
     *
     * According to the Remote Execution specification, this is not a
     problem:
     * the server must return the parts of the tree that are present and
     * ignore the rest.
     */

    // Creating `root/`:
    Directory root_directory;

    // Adding a file to it:
    FileNode *f1 = root_directory.add_files();
    f1->set_name("file1.sh");
    f1->set_is_executable(true);
    f1->mutable_digest()->CopyFrom(make_digest("file1Contents"));

    // Creating `subdir1/`:
    Directory subdirectory;

    // Adding `subdir1/file2.c`:
    FileNode *f3 = subdirectory.add_files();
    f3->set_name("file2.c");
    f3->mutable_digest()->CopyFrom(make_digest("file3Contents"));

    // Adding `subdir1/` under `dirA/`:
    DirectoryNode *d1 = root_directory.add_directories();
    d1->set_name("subdir1");

    const auto serialized_subdirectory = subdirectory.SerializeAsString();
    const auto subdirectory_digest = make_digest(serialized_subdirectory);
    d1->mutable_digest()->CopyFrom(subdirectory_digest);

    const auto serialized_root_directory = root_directory.SerializeAsString();
    const auto root_directory_digest = make_digest(serialized_root_directory);

    // We only store the root directory:
    remote_storage->writeBlob(root_directory_digest,
                              serialized_root_directory);

    // And request the tree:
    GetTreeRequest request;
    request.mutable_root_digest()->CopyFrom(root_directory_digest);

    auto reader = cas_stub->GetTree(&client_context, request);

    // We received a valid reply:
    GetTreeResponse response;
    ASSERT_TRUE(reader->Read(&response));

    // And it contains what we want:
    ASSERT_EQ(response.directories_size(), 1);

    const auto node = response.directories(0);
    ASSERT_EQ(node.directories_size(), 1);
    ASSERT_EQ(node.files_size(), 1);
    ASSERT_EQ(node.files(0).name(), "file1.sh");
    ASSERT_TRUE(node.files(0).is_executable());

    ASSERT_TRUE(local_storage->hasBlob(root_directory_digest));
}

TEST_F(CasProxyFixture, BytestreamReadWithInvalidResourceName)
{
    ReadRequest request;
    request.set_resource_name("/root/path/to/something");

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(),
              grpc::StatusCode::INVALID_ARGUMENT);
}

TEST_F(CasProxyFixture, BytestreamRemoteReadWithNegativeReadOffset)
{
    const auto digest = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(digest));

    ReadRequest request;
    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_offset(-1);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OUT_OF_RANGE);
}

TEST_F(CasProxyFixture, BytestreamRemoteReadWithReadOffsetExceedingRealSize)
{
    const std::string data = "data1";
    const auto digest = make_digest(data);
    ASSERT_TRUE(local_storage->hasBlob(digest));

    ReadRequest request;
    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_offset(
        static_cast<google::protobuf::int64>(data.size() + 1));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OUT_OF_RANGE);
}

TEST_F(CasProxyFixture, BytestreamLocalReadWithNegativeReadOffset)
{
    const auto digest = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(digest));

    ReadRequest request;
    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_offset(-1);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OUT_OF_RANGE);
}

TEST_F(CasProxyFixture, BytestreamLocalReadWithNegativeReadLimit)
{
    const std::string data = "data1";
    const Digest digest = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_limit(-3);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(),
              grpc::StatusCode::INVALID_ARGUMENT);
}

TEST_F(CasProxyFixture, BytestreamLocalReadWithReadOffsetExceedingRealSize)
{
    const std::string data = "data1";
    const auto digest = make_digest(data);
    ASSERT_TRUE(local_storage->hasBlob(digest));

    ReadRequest request;
    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_offset(
        static_cast<google::protobuf::int64>(data.size() + 1));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OUT_OF_RANGE);
}
TEST_F(CasProxyFixture, BytestreamLocalReadMissingBlob)
{
    const Digest digest = make_digest("data25");
    ASSERT_FALSE(local_storage->hasBlob(digest));

    ReadRequest request;
    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::NOT_FOUND);
}

TEST_F(CasProxyFixture, BytestreamLocalReadEmptyBlob)
{
    const Digest digest = make_digest("");

    local_storage->writeBlob(digest, "");
    ASSERT_TRUE(local_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), "");
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamLocalRead)
{
    const std::string data = "data1";
    const Digest digest = make_digest(data);
    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_FALSE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), data);
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamLocalLargeRead)
{
    // This blob will have to be split into multiple chunks due to the gRPC's
    // message size limit:
    std::string data;
    while (data.size() < 16 * 1024 * 1024) {
        data += "SomeData\0";
    }

    const Digest digest = make_digest(data);
    local_storage->writeBlob(digest, data);
    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_FALSE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    std::string fetched_data;
    while (reader->Read(&response)) {
        fetched_data += response.data();
    }
    ASSERT_EQ(fetched_data, data);

    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamLocalReadWithLimit)
{
    const std::string data = "data1";
    const Digest digest = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_FALSE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_limit(2);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), "da");
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamLocalReadWithOffset)
{
    const std::string data = "data1";
    const Digest digest = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_FALSE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_offset(3);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), "a1");
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamLocalReadWithOffsetAndLimit)
{
    const std::string data = "data1";
    const Digest digest = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_FALSE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_limit(2);
    request.set_read_offset(1);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), "at");
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamWriteWithInvalidResourceName)
{
    WriteRequest request;
    request.set_resource_name("/root/path/to/something");
    request.set_data("data");
    request.set_finish_write(true);

    WriteResponse response;
    auto writer = cas_bytestream_stub->Write(&client_context, &response);

    ASSERT_TRUE(writer->Write(request));
    ASSERT_EQ(response.committed_size(), 0);
    ASSERT_EQ(writer->Finish().error_code(),
              grpc::StatusCode::INVALID_ARGUMENT);
}

TEST_F(CasProxyFixture, BytestreamRemoteReadMissingBlob)
{
    const Digest digest = make_digest("data25");
    ASSERT_FALSE(local_storage->hasBlob(digest));
    ASSERT_FALSE(remote_storage->hasBlob(digest));

    ReadRequest request;
    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_FALSE(reader->Read(&response));
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::NOT_FOUND);
}

TEST_F(CasProxyFixture, BytestreamRemoteRead)
{
    const std::string data = "remotedata3";
    const Digest digest = make_digest(data);
    ASSERT_FALSE(local_storage->hasBlob(digest));
    ASSERT_TRUE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), data);
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);

    ASSERT_TRUE(local_storage->hasBlob(digest));
}

TEST_F(CasProxyFixture, BytestreamRemoteLargeRead)
{
    // This blob will have to be split into multiple chunks due to the gRPC's
    // message size limit:
    std::string data;
    while (data.size() < 16 * 1024 * 1024) {
        data += "SomeData\0";
    }

    const Digest digest = make_digest(data);
    remote_storage->writeBlob(digest, data);
    ASSERT_FALSE(local_storage->hasBlob(digest));
    ASSERT_TRUE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    std::string fetched_data;
    while (reader->Read(&response)) {
        fetched_data += response.data();
    }
    ASSERT_EQ(fetched_data, data);

    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamRemoteReadWithLimit)
{
    const std::string data = "remotedata3";
    const Digest digest = make_digest(data);
    ASSERT_FALSE(local_storage->hasBlob(digest));
    ASSERT_TRUE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_limit(2);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), "re");
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);

    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_EQ(local_storage->readBlob(digest), data);
}

TEST_F(CasProxyFixture, BytestreamRemoteReadWithOffset)
{
    const std::string data = "remotedata3";
    const Digest digest = make_digest(data);
    ASSERT_TRUE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_offset(6);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), "data3");
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);

    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_EQ(local_storage->readBlob(digest), data);
}

TEST_F(CasProxyFixture, BytestreamRemoteReadWithOffsetAndLimit)
{
    const std::string data = "data1";
    const Digest digest = make_digest("data1");
    ASSERT_TRUE(local_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()));

    request.set_read_limit(2);
    request.set_read_offset(1);

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), "at");
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamResourceNameIgnoreValuesAfterSize)
{
    const Digest digest = make_digest("remotedata3");
    ASSERT_FALSE(local_storage->hasBlob(digest));
    ASSERT_TRUE(remote_storage->hasBlob(digest));

    ReadRequest request;

    request.set_resource_name("blobs/" + digest.hash() + "/" +
                              std::to_string(digest.size_bytes()) +
                              "/extra/path/to/file.c");

    auto reader = cas_bytestream_stub->Read(&client_context, request);

    ReadResponse response;
    ASSERT_TRUE(reader->Read(&response));
    ASSERT_EQ(response.data(), local_storage->readBlob(digest));
    ASSERT_EQ(reader->Finish().error_code(), grpc::StatusCode::OK);
}

TEST_F(CasProxyFixture, BytestreamWrite)
{
    const std::string data = "data10";
    const Digest digest = make_digest(data);

    ASSERT_FALSE(local_storage->hasBlob(digest));
    ASSERT_FALSE(remote_storage->hasBlob(digest));

    WriteRequest request;
    request.set_resource_name("uploads/uuid-goes-here/blobs/" + digest.hash() +
                              "/" + std::to_string(digest.size_bytes()));
    request.set_data(data);
    request.set_finish_write(true);

    WriteResponse response;
    auto writer = cas_bytestream_stub->Write(&client_context, &response);

    ASSERT_TRUE(writer->Write(request));
    ASSERT_TRUE(writer->WritesDone());
    ASSERT_EQ(writer->Finish().error_code(), grpc::StatusCode::OK);
    ASSERT_EQ(response.committed_size(),
              local_storage->readBlob(digest).size());

    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_TRUE(remote_storage->hasBlob(digest));

    ASSERT_EQ(local_storage->readBlob(digest), data);
    ASSERT_EQ(remote_storage->readBlob(digest), data);
}

TEST_F(CasProxyFixture, BytestreamWriteMultiPart)
{
    const std::string data = "Part1|Part2";
    const Digest digest = make_digest(data);

    ASSERT_FALSE(local_storage->hasBlob(digest));
    ASSERT_FALSE(remote_storage->hasBlob(digest));

    const std::string resource_name = "uploads/uuid-goes-here/blobs/" +
                                      digest.hash() + "/" +
                                      std::to_string(digest.size_bytes());
    WriteRequest request1;
    request1.set_resource_name(resource_name);
    request1.set_data("Part1|");
    request1.set_finish_write(false);

    WriteRequest request2;
    request2.set_resource_name(resource_name);
    request2.set_data("Part2");
    request2.set_finish_write(true);

    WriteResponse response;
    auto writer = cas_bytestream_stub->Write(&client_context, &response);

    ASSERT_TRUE(writer->Write(request1));
    ASSERT_TRUE(writer->Write(request2));
    ASSERT_TRUE(writer->WritesDone());

    ASSERT_EQ(writer->Finish().error_code(), grpc::StatusCode::OK);
    ASSERT_EQ(response.committed_size(),
              local_storage->readBlob(digest).size());

    ASSERT_TRUE(local_storage->hasBlob(digest));
    ASSERT_TRUE(remote_storage->hasBlob(digest));

    ASSERT_EQ(local_storage->readBlob(digest), data);
    ASSERT_EQ(remote_storage->readBlob(digest), data);
}

TEST_F(CasProxyFixture, BytestreamWriteMultiPartWithoutFinishWriteFails)
{
    const std::string data = "Part1|Part2";
    const Digest digest = make_digest(data);

    const std::string resource_name = "uploads/uuid-goes-here/blobs/" +
                                      digest.hash() + "/" +
                                      std::to_string(digest.size_bytes());

    ASSERT_FALSE(local_storage->hasBlob(digest));

    WriteRequest request1;
    request1.set_resource_name(resource_name);
    request1.set_data("Part1|");
    request1.set_finish_write(false);

    WriteRequest request2;
    request2.set_resource_name(resource_name);
    request2.set_data("Part2");
    request2.set_finish_write(false);
    // ^ The last request should set `finish_write`. The write will fail.

    WriteResponse response;
    auto writer = cas_bytestream_stub->Write(&client_context, &response);

    ASSERT_TRUE(writer->Write(request1));
    ASSERT_TRUE(writer->Write(request2));
    ASSERT_TRUE(writer->WritesDone());

    ASSERT_EQ(writer->Finish().error_code(),
              grpc::StatusCode::INVALID_ARGUMENT);

    // The write was not committed:
    ASSERT_EQ(response.committed_size(), 0);
    ASSERT_FALSE(local_storage->hasBlob(digest));
}

TEST_F(CasProxyFixture, BytestreamWriteMultiPartInvalidDigestFails)
{
    const std::string data = "Part1|Part2";
    const Digest digest = make_digest(data);

    const std::string resource_name = "uploads/uuid-goes-here/blobs/" +
                                      digest.hash() + "/" +
                                      std::to_string(digest.size_bytes());
    WriteRequest request1;
    request1.set_resource_name(resource_name);
    request1.set_data("Part1|");
    request1.set_finish_write(false);

    WriteRequest request2;
    request2.set_resource_name(resource_name);
    request2.set_data("SomeOtherPart");
    request2.set_finish_write(true);

    WriteResponse response;
    auto writer = cas_bytestream_stub->Write(&client_context, &response);

    ASSERT_TRUE(writer->Write(request1));
    ASSERT_TRUE(writer->Write(request2));
    ASSERT_TRUE(writer->WritesDone());

    ASSERT_EQ(writer->Finish().error_code(),
              grpc::StatusCode::INVALID_ARGUMENT);
    ASSERT_EQ(response.committed_size(), 0);
}

TEST_F(CasProxyFixture, BytestreamWriteWithInvalidDigestFails)
{
    const std::string data = "data11";
    Digest digest;
    digest.set_hash("hash");

    const auto invalid_data_size =
        static_cast<google::protobuf::int64>(data.size() + 3);
    digest.set_size_bytes(invalid_data_size);

    WriteRequest request;
    request.set_resource_name("uploads/uuid-goes-here/blobs/" + digest.hash() +
                              "/" + std::to_string(digest.size_bytes()));
    request.set_data(data);
    request.set_finish_write(true);

    WriteResponse response;
    auto writer = cas_bytestream_stub->Write(&client_context, &response);

    ASSERT_TRUE(writer->Write(request));
    ASSERT_TRUE(writer->WritesDone());

    ASSERT_EQ(writer->Finish().error_code(),
              grpc::StatusCode::INVALID_ARGUMENT);
    ASSERT_EQ(response.committed_size(), 0);
}

TEST_F(CasProxyFixture, BytestreamWriteExtraRequestFails)
{
    const std::string data = "data10";
    const Digest digest = make_digest(data);

    const std::string resource_name = "uploads/uuid-goes-here/blobs/" +
                                      digest.hash() + "/" +
                                      std::to_string(digest.size_bytes());
    WriteRequest request1;
    request1.set_resource_name(resource_name);
    request1.set_data(data);
    request1.set_finish_write(true);
    // (After setting `finish_write` it is illegal to send more requests.)

    WriteRequest request2 = request1;

    WriteResponse response;
    auto writer = cas_bytestream_stub->Write(&client_context, &response);

    ASSERT_TRUE(writer->Write(request1));
    ASSERT_TRUE(writer->Write(request2));
    ASSERT_TRUE(writer->WritesDone());

    ASSERT_EQ(writer->Finish().error_code(),
              grpc::StatusCode::INVALID_ARGUMENT);
    ASSERT_EQ(response.committed_size(), 0);
}

TEST_F(CasProxyFixture, BytestreamWriteMismatchedRequests)
{
    const std::string data = "Part1|Part2";
    const Digest digest = make_digest(data);

    const std::string resource_name = "uploads/uuid-goes-here/blobs/" +
                                      digest.hash() + "/" +
                                      std::to_string(digest.size_bytes());
    WriteRequest request1;
    request1.set_resource_name(resource_name);
    request1.set_data("Part1|");
    request1.set_finish_write(false);

    WriteRequest request2;
    request2.set_resource_name(resource_name + "1");
    // (This changes the Digest, which must match in all requests.)

    request2.set_data("Part2");
    request2.set_finish_write(true);

    WriteResponse response;
    auto writer = cas_bytestream_stub->Write(&client_context, &response);

    ASSERT_TRUE(writer->Write(request1));
    ASSERT_TRUE(writer->Write(request2));
    ASSERT_TRUE(writer->WritesDone());

    ASSERT_EQ(writer->Finish().error_code(),
              grpc::StatusCode::INVALID_ARGUMENT);
    ASSERT_EQ(response.committed_size(), 0);
}
