/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LOCALCASSERVICE_H
#define INCLUDED_BUILDBOXCASD_LOCALCASSERVICE_H

#include <buildboxcasd_localcas.h>

#include <mutex>

#include <grpc/grpc.h>
#include <grpcpp/server.h>
#include <grpcpp/server_context.h>

#include <buildboxcasd_filestager.h>
#include <buildboxcasd_localcas.h>
#include <buildboxcommon_client.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_merklize.h>
#include <buildboxcommon_protos.h>

namespace buildboxcasd {

using namespace build::buildgrid;
using namespace build::bazel::remote::execution::v2;
using namespace google::protobuf;
using namespace buildboxcommon;

using grpc::ServerContext;
using grpc::ServerReaderWriter;
using grpc::Status;

class LocalCasServiceImplementation final
    : public LocalContentAddressableStorage::Service {
    /* Implements the LocalContentAddressableStorage RPC methods */

  public:
    LocalCasServiceImplementation(LocalCas *storage, Client *client = nullptr);

    Status FetchMissingBlobs(ServerContext *context,
                             const FetchMissingBlobsRequest *request,
                             FetchMissingBlobsResponse *response) override;

    Status UploadMissingBlobs(ServerContext *context,
                              const UploadMissingBlobsRequest *request,
                              UploadMissingBlobsResponse *response) override;

    Status FetchTree(ServerContext *context, const FetchTreeRequest *request,
                     FetchTreeResponse *response) override;

    Status UploadTree(ServerContext *context, const UploadTreeRequest *request,
                      UploadTreeResponse *response) override;

    Status StageTree(ServerContext *context,
                     ServerReaderWriter<StageTreeResponse, StageTreeRequest>
                         *stream) override;

    Status CaptureTree(ServerContext *context,
                       const CaptureTreeRequest *request,
                       CaptureTreeResponse *response) override;

    Status CaptureFiles(ServerContext *context,
                        const CaptureFilesRequest *request,
                        CaptureFilesResponse *response) override;

    Status GetInstanceNameForRemote(
        ServerContext *context, const GetInstanceNameForRemoteRequest *request,
        GetInstanceNameForRemoteResponse *response) override;

    Status GetLocalDiskUsage(ServerContext *context,
                             const GetLocalDiskUsageRequest *request,
                             GetLocalDiskUsageResponse *response) override;

  private:
    /*
     * Store digests locally, and upload to remote CAS.
     * Returns Tree digest for constructing the response.
     */
    const Digest
    UploadAndStore(buildboxcommon::digest_string_map *const digest_map,
                   const Tree &path_tree, bool bypass_local_cache,
                   Client *client);

    std::string createTemporaryDirectory() const;

    // Returns whether all the contents of given tree are stored locally.
    bool treeIsAvailableLocally(const Digest &root_digest) const;

    // Walks a tree and makes sure that all its blobs are stored locally.
    // If a CAS client is available, it will try and fetch missing blobs from a
    // remote.
    // It returns an `OK` status if all the blobs contained in the tree are
    // available locally, `FAILED_PRECONDITION` otherwise.
    Status prepareTreeForStaging(const Digest &root_digest,
                                 Client *client) const;

    // Fetches from a remote CAS the blobs in the given tree that are not
    // present locally.
    void fetchTreeMissingBlobs(const Digest &root_digest,
                               Client *client) const;

    std::vector<Digest>
    digestsMissingFromDirectory(const Directory &directory) const;

    Directory fetchDirectory(const Digest &root_digest, Client *client) const;

    Status
    stage(const Digest &root_digest, const std::string &stage_path,
          bool delete_directory_on_error,
          ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream);

    Client *getClientForInstanceName(const std::string &instance_name);

    LocalCas *d_storage;
    Client *d_client;
    FileStager d_hard_link_stager;

    std::map<std::string, std::unique_ptr<Client>> d_instance_client_map;
    std::mutex d_instance_client_map_mutex;
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LOCALCASSERVICE_H
