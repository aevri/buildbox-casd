/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_localcasservice.h>
#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_temporarydirectory.h>

namespace buildboxcasd {

using namespace build::buildgrid;
using namespace google::protobuf::util;
using namespace google::protobuf;
using namespace buildboxcommon;

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerReaderWriter;
using grpc::ServerWriter;
using grpc::Status;

/*
 *   Helper method that stores err in status, and logs the message.
 */
void logAndStoreMessage(const std::string &err, google::rpc::Status &status,
                        grpc::StatusCode code)
{
    status.set_message(err);
    status.set_code(code);
    BUILDBOX_LOG_ERROR(err);
}

LocalCasServiceImplementation::LocalCasServiceImplementation(LocalCas *storage,
                                                             Client *client)
    : d_storage(storage), d_client(client), d_hard_link_stager(storage)
{
}

Client *LocalCasServiceImplementation::getClientForInstanceName(
    const std::string &instance_name)
{
    if (instance_name.empty()) {
        return d_client;
    }

    std::lock_guard<std::mutex> lock(d_instance_client_map_mutex);

    auto it = d_instance_client_map.find(instance_name);
    if (it == d_instance_client_map.end()) {
        return nullptr;
    }

    return it->second.get();
}

Status LocalCasServiceImplementation::FetchMissingBlobs(
    ServerContext *, const FetchMissingBlobsRequest *request,
    FetchMissingBlobsResponse *response)
{
    // Checking the local storage first:
    std::vector<Digest> digests_missing_locally;
    for (const Digest &digest : request->blob_digests()) {
        if (!d_storage->hasBlob(digest)) {
            digests_missing_locally.push_back(digest);
        }
    }

    if (digests_missing_locally.empty()) {
        return grpc::Status::OK;
    }

    auto client = getClientForInstanceName(request->instance_name());
    if (client == nullptr) {
        return grpc::Status(grpc::StatusCode::UNAVAILABLE,
                            "No remote configured");
    }

    // Making a request for the blobs that we couldn't find locally:
    const buildboxcommon::Client::DownloadedData downloaded_data =
        client->downloadBlobs(digests_missing_locally);

    for (const Digest &digest : digests_missing_locally) {
        google::rpc::Status status;
        status.set_code(grpc::StatusCode::OK);

        const auto digest_data_it = downloaded_data.find(digest.hash());
        if (digest_data_it != downloaded_data.cend()) {
            const std::string data = digest_data_it->second;

            try {
                d_storage->writeBlob(digest, data);
            }
            catch (const std::runtime_error &e) {
                status.set_code(grpc::StatusCode::INTERNAL);
                status.set_message(
                    "Internal error while writing blob to local CAS: " +
                    std::string(e.what()));
            }
        }
        else {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob could not be fetched from remote CAS");
        }

        if (status.code() != grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(status);
        }
    }

    return grpc::Status::OK;
}

Status LocalCasServiceImplementation::UploadMissingBlobs(
    ServerContext *, const UploadMissingBlobsRequest *request,
    UploadMissingBlobsResponse *response)
{
    auto client = getClientForInstanceName(request->instance_name());
    if (client == nullptr) {
        return grpc::Status(grpc::StatusCode::UNAVAILABLE,
                            "No remote configured");
    }

    // Construct std::vector from protobuf digest list
    auto pb_digests = request->blob_digests();
    std::vector<Digest> digests(pb_digests.begin(), pb_digests.end());

    const auto missing_digests = client->findMissingBlobs(digests);

    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    for (const Digest &digest : missing_digests) {
        google::rpc::Status status;
        status.set_code(grpc::StatusCode::OK);

        try {
            const auto data = d_storage->readBlob(digest);
            upload_requests.emplace_back(
                buildboxcommon::Client::UploadRequest(digest, data));
        }
        catch (const BlobNotFoundException &) {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob not found in local cache");
        }
        catch (const std::runtime_error &e) {
            status.set_code(grpc::StatusCode::INTERNAL);
            status.set_message(e.what());
        }

        if (status.code() != grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(status);
        }
    }

    const auto not_uploaded_blobs = client->uploadBlobs(upload_requests);

    for (const buildboxcommon::Client::UploadResult &upload_result :
         not_uploaded_blobs) {
        google::rpc::Status status;
        status.set_code(upload_result.status.error_code());
        status.set_message(upload_result.status.error_message());

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(upload_result.digest);
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

Status LocalCasServiceImplementation::FetchTree(ServerContext *,
                                                const FetchTreeRequest *,
                                                FetchTreeResponse *)
{
    return grpc::Status(grpc::StatusCode::UNIMPLEMENTED, __func__);
}

Status LocalCasServiceImplementation::UploadTree(ServerContext *,
                                                 const UploadTreeRequest *,
                                                 UploadTreeResponse *)
{
    return grpc::Status(grpc::StatusCode::UNIMPLEMENTED, __func__);
}

Status LocalCasServiceImplementation::StageTree(
    ServerContext *,
    ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
{
    StageTreeRequest stage_request;
    if (!stream->Read(&stage_request)) {
        return Status(grpc::StatusCode::INTERNAL, "Could not read request.");
    }

    Client *client = getClientForInstanceName(stage_request.instance_name());

    // Before we can stage the tree, we need to make sure that all of its
    // contents are stored locally.
    const auto tree_availability_status =
        prepareTreeForStaging(stage_request.root_digest(), client);
    if (!tree_availability_status.ok()) {
        return tree_availability_status;
    }
    // All the blobs required to stage are present in the local storage.

    /* There are 3 cases for `stage_request.path()`:
     *  a) Empty string (we need to create a temporary directory),
     *  b) A path to a directory that doesn't exist yet
     *  (and that will be created by the stager), or
     *  c) A path to an empty directory.
     *
     * In cases a) and b), cleanup involves deleting the directory. But for c)
     * we just want to empty it.
     */

    bool stage_request_path_exists;
    std::string stage_path;
    if (stage_request.path().empty()) {
        stage_request_path_exists = false;

        try {
            stage_path = createTemporaryDirectory();
        }
        catch (const std::system_error &e) {
            std::ostringstream error_message;
            error_message << "Could not create temporary directory to stage: "
                          << stage_request.root_digest() << ": " << e.what();

            BUILDBOX_LOG_ERROR(error_message.str());
            return Status(grpc::StatusCode::INTERNAL, error_message.str());
        }
    }
    else {
        stage_request_path_exists = buildboxcommon::FileUtils::is_directory(
            stage_request.path().c_str());
        stage_path = stage_request.path();
    }

    // Stage the files with the stage method which will also write back
    // onto the stream to the client to inform them that the staging is
    // done.
    Status stage_status = stage(stage_request.root_digest(), stage_path,
                                !stage_request_path_exists, stream);
    if (!stage_status.ok()) {
        return stage_status;
    }

    // The staging operation was successful.
    // Now we wait for a second request, which we expect to be empty:
    StageTreeRequest cleanup_request;
    const bool received_cleanup_request = stream->Read(&cleanup_request);

    if (received_cleanup_request &&
        (!cleanup_request.path().empty() ||
         !(cleanup_request.root_digest() == Digest()))) {
        std::ostringstream error_message;
        error_message << "Unexpected non-empty request after staging "
                      << stage_request.root_digest() << " in " << stage_path;
        stage_status =
            Status(grpc::StatusCode::INVALID_ARGUMENT, error_message.str());
    }

    try {
        d_hard_link_stager.unstage(stage_path, !stage_request_path_exists);
    }
    catch (const std::system_error &e) {
        BUILDBOX_LOG_WARNING("Caught " << e.what() << " while unstaging ["
                                       << stage_path);
    }

    // Sending an empty reply that signals that we are done cleaning:
    if (received_cleanup_request) {
        stream->Write(StageTreeResponse());
    }
    return stage_status;
}

bool LocalCasServiceImplementation::treeIsAvailableLocally(
    const Digest &root_digest) const
{
    Directory directory;
    if (!d_storage->hasBlob(root_digest) ||
        !directory.ParseFromString(d_storage->readBlob(root_digest))) {
        return false;
    }

    for (const FileNode &file : directory.files()) {
        if (!this->d_storage->hasBlob(file.digest())) {
            return false;
        }
    }

    for (const DirectoryNode &dir : directory.directories()) {
        if (!treeIsAvailableLocally(dir.digest())) {
            return false;
        }
    }

    return true;
}

Status
LocalCasServiceImplementation::prepareTreeForStaging(const Digest &root_digest,
                                                     Client *client) const
{
    // Server mode. (All the blobs must be available locally.)
    if (client == nullptr) {
        if (treeIsAvailableLocally(root_digest)) {
            return grpc::Status::OK;
        }
        return Status(grpc::StatusCode::FAILED_PRECONDITION,
                      "Tree is not completely available from LocalCAS.");
    }

    // Proxy mode. (We can try and fetch the blobs that might be missing.)
    try {
        fetchTreeMissingBlobs(root_digest, client);
        return grpc::Status::OK;
    }
    catch (const std::runtime_error &e) {
        const std::string error_message =
            "Error while fetching blobs that are missing locally for staging "
            "directory with root digest \"" +
            toString(root_digest) + "\": " + e.what();
        BUILDBOX_LOG_ERROR(error_message);
        return Status(grpc::StatusCode::FAILED_PRECONDITION, error_message);
    }
}

void LocalCasServiceImplementation::fetchTreeMissingBlobs(
    const Digest &root_digest, Client *client) const
{
    assert(client != nullptr);

    const Directory directory = fetchDirectory(root_digest, client);
    // (This `fetchDirectory()` throws if it fails, in which case we'll abort.)

    const std::vector<Digest> missing_digests =
        digestsMissingFromDirectory(directory);
    const auto downloaded_blobs = client->downloadBlobs(missing_digests);

    for (const Digest &digest : missing_digests) {
        if (downloaded_blobs.count(digest.hash()) == 0) {
            // If a single blob is missing we can't stage the directory,
            // aborting:
            throw std::runtime_error(
                "Could not fetch missing blob with digest \"" +
                toString(digest) + "\"");
        }
        this->d_storage->writeBlob(digest, downloaded_blobs.at(digest.hash()));
    }

    // Recursively fetching the missing blobs in each subdirectory:
    for (const DirectoryNode &dir : directory.directories()) {
        fetchTreeMissingBlobs(dir.digest(), client);
    }
}

Directory
LocalCasServiceImplementation::fetchDirectory(const Digest &root_digest,
                                              Client *client) const
{
    Directory directory;
    if (this->d_storage->hasBlob(root_digest) &&
        directory.ParseFromString(this->d_storage->readBlob(root_digest))) {
        return directory;
    }

    if (client == nullptr) {
        throw std::runtime_error("No CAS client configured. Could not "
                                 "fetch tree's missing blobs.");
    }

    const std::string directory_blob = client->fetchString(root_digest);
    if (!directory.ParseFromString(directory_blob)) {
        throw std::runtime_error(
            "Error parsing fetched Directory with digest " +
            toString(root_digest));
    }

    d_storage->writeBlob(root_digest, directory.SerializeAsString());
    return directory;
}

std::vector<Digest> LocalCasServiceImplementation::digestsMissingFromDirectory(
    const Directory &directory) const
{
    std::vector<Digest> missing_digests;
    missing_digests.reserve(static_cast<size_t>(directory.files_size()) +
                            static_cast<size_t>(directory.directories_size()));

    for (const FileNode &file : directory.files()) {
        if (!this->d_storage->hasBlob(file.digest())) {
            missing_digests.push_back(file.digest());
        }
    }
    for (const DirectoryNode &dir : directory.directories()) {
        if (!this->d_storage->hasBlob(dir.digest())) {
            missing_digests.push_back(dir.digest());
        }
    }
    missing_digests.shrink_to_fit();
    return missing_digests;
}

Status LocalCasServiceImplementation::stage(
    const Digest &root_digest, const std::string &stage_path,
    bool delete_directory_on_error,
    ServerReaderWriter<StageTreeResponse, StageTreeRequest> *stream)
{
    Status stage_status;
    try {
        d_hard_link_stager.stage(root_digest, stage_path);

        StageTreeResponse response;
        response.set_path(stage_path);
        stream->Write(response);

        stage_status = grpc::Status::OK;
    }
    catch (const std::invalid_argument &e) {
        stage_status = Status(grpc::StatusCode::INVALID_ARGUMENT, e.what());
    }
    catch (const std::runtime_error &e) {
        stage_status = Status(grpc::StatusCode::INTERNAL, e.what());
    }

    if (!stage_status.ok() && delete_directory_on_error) {
        // `stage()` rolls back the status of `stage_path` when the
        // operation aborts, leaving it as it originally was.
        // However, if we created a temporary directory, we want to delete
        // it.
        try {
            buildboxcommon::FileUtils::delete_directory(stage_path.c_str());
        }
        catch (const std::system_error &e) {
            BUILDBOX_LOG_ERROR("Could not delete directory "
                               << stage_path << ":" << e.what());
        }
    }

    return stage_status;
}

std::string LocalCasServiceImplementation::createTemporaryDirectory() const
{
    buildboxcommon::TemporaryDirectory stage_directory =
        d_storage->createTemporaryDirectory();
    stage_directory.setAutoRemove(false);
    return std::string(stage_directory.name());
}

Status
LocalCasServiceImplementation::CaptureTree(ServerContext *,
                                           const CaptureTreeRequest *request,
                                           CaptureTreeResponse *response)
{
    for (const std::string &path : request->path()) {

        auto resp = response->add_responses();
        google::rpc::Status status;
        std::string error_message;

        if (path.empty() || !FileUtils::is_directory(path.c_str()) ||
            path[0] != '/') {
            error_message = "Path: " + path +
                            " is empty, not a directory, or not an absolute "
                            "path.";
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::NOT_FOUND);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }

        digest_string_map digest_map;
        Tree path_tree;
        NestedDirectory nest_dir;
        Digest tree_digest;

        try {
            nest_dir = make_nesteddirectory(path.c_str(), &digest_map);
            path_tree = nest_dir.to_tree();
        }
        // Catch system errors thrown in make_nested.
        catch (const std::system_error &e) {
            error_message = string(e.what()) + " thrown for path: " + path +
                            " in make_nesteddirectory";
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::INTERNAL);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }

        // Upload to remote CAS and store locally if bypass_local_cache() is
        // not set
        nest_dir.to_digest(&digest_map);
        try {
            auto client = getClientForInstanceName(request->instance_name());
            tree_digest = UploadAndStore(
                &digest_map, path_tree, request->bypass_local_cache(), client);
        }
        catch (const OutOfSpaceException &e) {
            error_message = string(e.what()) + " thrown for path: " + path +
                            " in writeBlob";
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::RESOURCE_EXHAUSTED);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }
        catch (const std::runtime_error &e) {
            error_message = string(e.what()) + " thrown for path: " + path +
                            " in writeBlob";
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::INTERNAL);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }
        catch (const std::invalid_argument &e) {
            error_message = string(e.what()) + " thrown for path: " + path +
                            " in writeBlob";
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::INTERNAL);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }

        resp->mutable_tree_digest()->CopyFrom(tree_digest);
        resp->set_path(path);

        status.set_code(grpc::StatusCode::OK);
        resp->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

Status
LocalCasServiceImplementation::CaptureFiles(ServerContext *,
                                            const CaptureFilesRequest *request,
                                            CaptureFilesResponse *response)
{
    for (const std::string &path : request->path()) {

        auto resp = response->add_responses();
        google::rpc::Status status;
        std::string error_message;

        if (path.empty() || FileUtils::is_directory(path.c_str())) {
            error_message = "Path: " + path + " is empty, or is a directory.";
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::NOT_FOUND);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }
        try {
            const std::string file_contents =
                FileUtils::get_file_contents(path.c_str());
            const Digest file_digest =
                buildboxcommon::make_digest(file_contents);

            auto client = getClientForInstanceName(request->instance_name());
            if (client != nullptr) {
                client->upload(file_contents, file_digest);
            }

            if (!request->bypass_local_cache()) {
                try {
                    d_storage->writeBlob(file_digest, file_contents);
                }
                catch (const OutOfSpaceException &e) {
                    error_message = string(e.what()) +
                                    " thrown for path: " + path +
                                    " in writeBlob";
                    logAndStoreMessage(error_message, status,
                                       grpc::StatusCode::RESOURCE_EXHAUSTED);
                    resp->mutable_status()->CopyFrom(status);
                    continue;
                }
                catch (const std::runtime_error &e) {
                    error_message = string(e.what()) +
                                    " thrown for path: " + path +
                                    " in writeBlob";
                    logAndStoreMessage(error_message, status,
                                       grpc::StatusCode::INTERNAL);
                    resp->mutable_status()->CopyFrom(status);
                    continue;
                }
                catch (const std::invalid_argument &e) {
                    error_message = string(e.what()) +
                                    " thrown for path: " + path +
                                    " in writeBlob";
                    logAndStoreMessage(error_message, status,
                                       grpc::StatusCode::INTERNAL);
                    resp->mutable_status()->CopyFrom(status);
                    continue;
                }
            }

            resp->mutable_digest()->CopyFrom(file_digest);
            resp->set_path(path);
            status.set_code(grpc::StatusCode::OK);
        }
        catch (const std::runtime_error &e) {
            error_message = string(e.what()) + " thrown for path: " + path;
            logAndStoreMessage(error_message, status,
                               grpc::StatusCode::INTERNAL);
            resp->mutable_status()->CopyFrom(status);
            continue;
        }
        resp->mutable_status()->CopyFrom(status);
    }
    return grpc::Status::OK;
}

Status LocalCasServiceImplementation::GetInstanceNameForRemote(
    ServerContext *, const GetInstanceNameForRemoteRequest *request,
    GetInstanceNameForRemoteResponse *response)
{
    /* Use hash of request message as instance name */
    const std::string request_str = request->SerializeAsString();
    const Digest request_digest = CASHash::hash(request_str);
    const std::string instance_name = request_digest.hash();

    std::lock_guard<std::mutex> lock(d_instance_client_map_mutex);

    if (d_instance_client_map.find(instance_name) ==
        d_instance_client_map.end()) {
        /* First request for this remote, instantiate client. */

        auto connection_options = buildboxcommon::ConnectionOptions();
        connection_options.d_url = request->url().c_str();
        connection_options.d_instanceName = request->instance_name().c_str();
        connection_options.d_serverCert = request->server_cert().c_str();
        connection_options.d_clientKey = request->client_key().c_str();
        connection_options.d_clientCert = request->client_cert().c_str();

        auto client = std::make_unique<Client>();
        client->init(connection_options);

        d_instance_client_map[instance_name] = std::move(client);
    }

    response->set_instance_name(instance_name);

    return grpc::Status::OK;
}

Status LocalCasServiceImplementation::GetLocalDiskUsage(
    ServerContext *, const GetLocalDiskUsageRequest *,
    GetLocalDiskUsageResponse *response)
{
    response->set_size_bytes(d_storage->getDiskUsage());
    response->set_quota_bytes(d_storage->getDiskQuota());

    return grpc::Status::OK;
}

const Digest LocalCasServiceImplementation::UploadAndStore(
    buildboxcommon::digest_string_map *const digest_map, const Tree &t,
    bool bypass_local_cache, Client *client)
{
    std::vector<Client::UploadRequest> upload_vec;
    // vector size is digest_map + 1 for tree_digest
    upload_vec.reserve(digest_map->size() + 1);

    // If bypass_local_cache is not set, writes digest to localCas
    auto storeDigestsLocally = [&](auto digest, auto content) {
        if (!bypass_local_cache) {
            d_storage->writeBlob(digest, content);
        }
    };

    Directory d;
    for (const auto &it : *digest_map) {
        // Check if string is a directory or file.Directory d;
        if (d.ParseFromString(it.second)) {
            upload_vec.emplace_back(
                Client::UploadRequest(it.first, it.second));
            storeDigestsLocally(it.first, it.second);
        }
        else {
            std::string file_content =
                FileUtils::get_file_contents(it.second.c_str());
            upload_vec.emplace_back(
                Client::UploadRequest(it.first, file_content));
            storeDigestsLocally(it.first, file_content);
        }
    }

    // Get and emplace back tree_digest.
    const Digest tree_digest = buildboxcommon::make_digest(t);
    const std::string tree_message_string = t.SerializeAsString();
    upload_vec.emplace_back(
        Client::UploadRequest(tree_digest, tree_message_string));

    storeDigestsLocally(tree_digest, tree_message_string);

    // upload to remote cas
    if (client != nullptr) {
        client->uploadBlobs(upload_vec);
    }

    return tree_digest;
}
} // namespace buildboxcasd
