/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_lrulocalcas.h>

#include <queue>
#include <stdexcept>

#include <buildboxcommon_logging.h>

using namespace buildboxcasd;

LruLocalCas::LruLocalCas(std::unique_ptr<LocalCas> base_cas, int64_t quota_low,
                         int64_t quota_high)
    : d_base_cas(std::move(base_cas)), d_quota_low(quota_low),
      d_quota_high(quota_high), d_protect_active_blobs(false)
{
    BUILDBOX_LOG_INFO("Initializing LRU with quota: " << quota_low << "/"
                                                      << quota_high);

    d_disk_usage = d_base_cas->getDiskUsage();
}

void LruLocalCas::protectActiveBlobs(blob_time_type start_time)
{
    d_protect_active_blobs = true;
    d_protect_active_time = start_time;
}

bool LruLocalCas::hasBlob(const Digest &digest)
{
    return d_base_cas->hasBlob(digest);
}

bool LruLocalCas::writeBlob(const Digest &digest, const std::string &data)
{
    const auto digest_size_bytes = digest.size_bytes();

    if (digest_size_bytes >= d_quota_high) {
        // Blob is larger than quota. Do not attempt cleanup and bail out.
        const std::string error_message = "Insufficient storage quota";
        BUILDBOX_LOG_ERROR(error_message);
        throw OutOfSpaceException(error_message);
    }

    // Reserve disk usage before actual write to ensure expiry is triggered
    // early enough in case of parallel write requests.
    d_disk_usage += digest_size_bytes;

    try {
        if (d_disk_usage >= d_quota_high) {
            // High watermark reached. Trigger expiry and wait for it to
            // complete.
            cleanup();
        }

        if (d_base_cas->writeBlob(digest, data)) {
            return true;
        }
        else {
            // No error but disk usage has not increased.
            d_disk_usage -= digest_size_bytes;
            return false;
        }
    }
    catch (...) {
        d_disk_usage -= digest_size_bytes;
        throw;
    }
}

std::string LruLocalCas::readBlob(const Digest &digest, size_t offset,
                                  size_t length)
{
    return d_base_cas->readBlob(digest, offset, length);
}

std::string LruLocalCas::readBlob(const Digest &digest)
{
    return d_base_cas->readBlob(digest);
}

bool LruLocalCas::deleteBlob(const Digest &digest)
{
    if (d_base_cas->deleteBlob(digest)) {
        d_disk_usage -= static_cast<unsigned long>(digest.size_bytes());
        return true;
    }
    else {
        return false;
    }
}

void LruLocalCas::listBlobs(digest_time_callback_type callback)
{
    d_base_cas->listBlobs(callback);
}

std::string LruLocalCas::path(const Digest &digest)
{
    return d_base_cas->path(digest);
}

buildboxcommon::TemporaryDirectory LruLocalCas::createTemporaryDirectory()
{
    return d_base_cas->createTemporaryDirectory();
}

int64_t LruLocalCas::getDiskUsage() { return d_disk_usage; }

int64_t LruLocalCas::getDiskQuota()
{
    if (d_quota_high == INT64_MAX) {
        // The value of 0 means no quota.
        return 0;
    }
    else {
        return d_quota_high;
    }
}

void LruLocalCas::verifyDiskUsage()
{
    auto actual_disk_usage = d_base_cas->getDiskUsage();

    if (d_disk_usage != actual_disk_usage) {
        std::stringstream error_message;
        error_message << "Cached disk usage " << d_disk_usage
                      << " does not match calculated disk usage "
                      << actual_disk_usage;
        throw std::runtime_error(error_message.str());
    }
}

void LruLocalCas::cleanup()
{
    std::lock_guard<std::mutex> lock(d_cleanup_mutex);

    if (d_disk_usage < d_quota_high) {
        // Another thread beat us to it, don't trigger another cleanup
        return;
    }

    BUILDBOX_LOG_INFO("Starting LRU cleanup. Disk usage: " << d_disk_usage);

    using time_digest_pair = std::pair<blob_time_type, Digest>;

    auto pq_compare = [](time_digest_pair &a, time_digest_pair &b) {
        return a.first > b.first;
    };

    std::priority_queue<time_digest_pair, std::vector<time_digest_pair>,
                        decltype(pq_compare)>
        pq(pq_compare);

    listBlobs([&pq](const buildboxcommon::Digest &digest,
                    buildboxcasd::blob_time_type time) {
        pq.push(std::make_pair(time, digest));
    });

    while (d_disk_usage > d_quota_low && !pq.empty()) {
        if (d_protect_active_blobs) {
            auto blob_time = pq.top().first;
            if (blob_time >= d_protect_active_time) {
                // Blob was created or used after the start point of active
                // blob protection. Do not delete blob.

                if (d_disk_usage >= d_quota_high) {
                    // Disk usage still above high watermark. This means that
                    // the working set is larger than the quota and we can't
                    // continue.

                    const std::string error_message =
                        "Insufficient storage quota";
                    BUILDBOX_LOG_ERROR(error_message);
                    throw OutOfSpaceException(error_message);
                }

                break;
            }
        }

        auto digest = pq.top().second;
        pq.pop();

        try {
            deleteBlob(digest);
        }
        catch (const std::runtime_error &e) {
            BUILDBOX_LOG_ERROR("Error deleting blob " << digest.hash() << "/"
                                                      << digest.size_bytes()
                                                      << ": " << e.what());
        }
    }

    BUILDBOX_LOG_INFO("Finished LRU cleanup. Disk usage: " << d_disk_usage);
}

LocalCas *LruLocalCas::getBase() { return d_base_cas.get(); }
