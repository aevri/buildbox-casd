/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_LRULOCALCAS_H
#define INCLUDED_BUILDBOXCASD_LRULOCALCAS_H

#include <atomic>
#include <chrono>
#include <mutex>

#include <buildboxcasd_localcas.h>
#include <buildboxcommon_client.h>
#include <buildboxcommon_protos.h>

namespace buildboxcasd {

class LruLocalCas final : public LocalCas {
    /* A LocalCas wrapper implementing LRU expiry of blobs. */
  public:
    LruLocalCas(std::unique_ptr<LocalCas> base_cas, int64_t quota_low,
                int64_t quota_high);
    LruLocalCas(std::unique_ptr<LocalCas> base_cas, int64_t quota_low,
                int64_t quota_high, blob_time_type protect_active_time);

    void protectActiveBlobs(blob_time_type start_time);

    bool hasBlob(const Digest &digest) override;

    bool writeBlob(const Digest &digest, const std::string &data) override;

    std::string readBlob(const Digest &digest, size_t offset,
                         size_t length) override;

    std::string readBlob(const Digest &digest) override;

    bool deleteBlob(const Digest &digest) override;

    void listBlobs(digest_time_callback_type callback) override;

    std::string path(const Digest &digest) override;

    buildboxcommon::TemporaryDirectory createTemporaryDirectory() override;

    int64_t getDiskUsage() override;

    int64_t getDiskQuota() override;

    void verifyDiskUsage();

    LocalCas *getBase();

  private:
    std::unique_ptr<LocalCas> d_base_cas;
    std::atomic<int64_t> d_disk_usage;
    int64_t d_quota_low;
    int64_t d_quota_high;
    bool d_protect_active_blobs;
    blob_time_type d_protect_active_time;
    std::mutex d_cleanup_mutex;

    int64_t calculateDiskUsage();

    void cleanup();
};

} // namespace buildboxcasd

#endif // INCLUDED_BUILDBOXCASD_LRULOCALCAS_H
