/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_daemon.h>

#include <exception>
#include <fcntl.h>
#include <google/rpc/code.pb.h>
#include <grpcpp/channel.h>
#include <signal.h>
#include <stdexcept>
#include <string>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <system_error>
#include <thread>
#include <unistd.h>

#include <grpcpp/server.h>
#include <grpcpp/server_builder.h>
#include <grpcpp/server_context.h>

#include <buildboxcommon_client.h>
#include <buildboxcommon_logging.h>

#include <buildboxcasd_casserver.h>
#include <buildboxcasd_lrulocalcas.h>

namespace buildboxcasd {

void Daemon::runDaemon()
{
    BUILDBOX_LOG_SET_LEVEL(d_log_level);

    BUILDBOX_LOG_INFO("Starting CASd instance \"" << this->d_instance_name
                                                  << "\" with cache at "
                                                  << this->d_local_cache_path);

    // Creating an FsLocalCas instance:
    std::unique_ptr<LocalCas> fs_storage =
        std::make_unique<FsLocalCas>(this->d_local_cache_path);

    std::shared_ptr<LocalCas> storage;

    if (d_quota_high == 0) {
        // No LRU expiry
        d_quota_low = d_quota_high = INT64_MAX;
    }

    // Creating LocalCas wrapper for LRU expiry
    auto lru_storage = std::make_shared<LruLocalCas>(
        std::move(fs_storage), d_quota_low, d_quota_high);

    if (d_protect_session_blobs) {
        // Do not delete blobs created or used in this session
        lru_storage->protectActiveBlobs(std::chrono::system_clock::now());
    }

    storage = lru_storage;

    std::shared_ptr<CasService> cas_server;
    if (d_cas_server.d_url) { // CAS proxy
        BUILDBOX_LOG_INFO("Creating CAS proxy server, remote CAS address = \""
                          << d_cas_server.d_url << "\"");
        auto remote_cas_client = std::make_shared<buildboxcommon::Client>();
        remote_cas_client->init(d_cas_server);
        cas_server = std::make_shared<CasService>(storage, remote_cas_client,
                                                  this->d_instance_name);
    }
    else { // Local CAS server
        BUILDBOX_LOG_DEBUG("Creating LocalCAS server");
        cas_server =
            std::make_shared<CasService>(storage, this->d_instance_name);
    }

    grpc::ServerBuilder builder;

    if (d_bind_address.empty()) {
        d_bind_address = "unix:" + d_local_cache_path + "/casd.sock";
    }

    builder.AddListeningPort(d_bind_address,
                             grpc::InsecureServerCredentials());
    builder.RegisterService(cas_server->remoteExecutionCasServicer().get());
    builder.RegisterService(cas_server->bytestreamServicer().get());
    builder.RegisterService(cas_server->localCasServicer().get());
    builder.RegisterService(cas_server->capabilitiesServicer().get());

    // Ignore SIGPIPE in case of using sockets + grpc without MSG_NOSIGNAL
    // support configured
    struct sigaction new_sig;
    new_sig.sa_handler = SIG_IGN;
    new_sig.sa_flags = 0;
    sigfillset(&new_sig.sa_mask);
    if (sigaction(SIGPIPE, &new_sig, NULL) == -1) {
        BUILDBOX_LOG_ERROR("Failed to set SIGPIPE handler");
        exit(1);
    }

    std::unique_ptr<grpc::Server> server(builder.BuildAndStart());
    BUILDBOX_LOG_INFO("Server listening on " << d_bind_address);
    server->Wait();
}

} // namespace buildboxcasd
