/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_casserver.h>

namespace buildboxcasd {

using namespace build::bazel::remote::execution::v2;
using namespace google::bytestream;
using namespace google::protobuf::util;
using namespace google::protobuf;

using grpc::ServerContext;
using grpc::ServerReader;
using grpc::ServerWriter;
using grpc::Status;

CasService::CasService(std::shared_ptr<LocalCas> cas_storage,
                       const std::string &instance_name)
    : d_cas_storage(cas_storage),
      d_cas(std::make_shared<LocalCasInstance>(d_cas_storage, instance_name)),
      d_remote_execution_cas_servicer(
          std::make_shared<CasRemoteExecutionServicer>(d_cas)),
      d_cas_bytestream_servicer(
          std::make_shared<CasBytestreamServicer>(d_cas)),
      d_local_cas_servicer(std::make_shared<LocalCasServiceImplementation>(
          d_cas_storage.get())),
      d_capabilities_servicer(
          std::make_shared<CapabilitiesServicer>(instance_name))
{
}

CasService::CasService(std::shared_ptr<LocalCas> cas_storage,
                       std::shared_ptr<buildboxcommon::Client> cas_client,
                       const std::string &instance_name)
    : d_cas_storage(cas_storage),
      d_cas(std::make_shared<LocalCasProxyInstance>(d_cas_storage, cas_client,
                                                    instance_name)),
      d_remote_execution_cas_servicer(
          std::make_shared<CasRemoteExecutionServicer>(d_cas)),
      d_cas_bytestream_servicer(
          std::make_shared<CasBytestreamServicer>(d_cas)),
      d_local_cas_servicer(std::make_shared<LocalCasServiceImplementation>(
          d_cas_storage.get(), cas_client.get())),
      d_capabilities_servicer(
          std::make_shared<CapabilitiesServicer>(instance_name))
{
}

/*
 * Remote Execution API: Content Addressable Storage service
 *
 */
CasRemoteExecutionServicer::CasRemoteExecutionServicer(
    std::shared_ptr<CasInstance> cas)
    : d_cas(cas)
{
}

#define CALL_IF_INSTANCE_NAME_MATCHES(servicer_function)                      \
    if (this->d_cas->instanceName() == request->instance_name()) {            \
        return servicer_function;                                             \
    }                                                                         \
    else {                                                                    \
        return INVALID_INSTANCE_NAME_ERROR(request)                           \
    }

#define INVALID_INSTANCE_NAME_ERROR(request)                                  \
    grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,                          \
                 "Invalid instance name \"" + request->instance_name() +      \
                     "\"");

Status CasRemoteExecutionServicer::FindMissingBlobs(
    ServerContext *, const FindMissingBlobsRequest *request,
    FindMissingBlobsResponse *response)
{
    CALL_IF_INSTANCE_NAME_MATCHES(d_cas->FindMissingBlobs(*request, response));
}

Status CasRemoteExecutionServicer::BatchUpdateBlobs(
    ServerContext *, const BatchUpdateBlobsRequest *request,
    BatchUpdateBlobsResponse *response)
{
    CALL_IF_INSTANCE_NAME_MATCHES(d_cas->BatchUpdateBlobs(*request, response));
}

Status CasRemoteExecutionServicer::BatchReadBlobs(
    ServerContext *, const BatchReadBlobsRequest *request,
    BatchReadBlobsResponse *response)
{

    // Checking that the blobs returned in the response do not exceed the gRPC
    // message size limit. We use the upper bound set in the CAS client.
    const auto response_size_limit = static_cast<google::protobuf::int64>(
        buildboxcommon::Client::bytestreamChunkSizeBytes());

    google::protobuf::int64 bytes_requested = 0;
    for (const Digest &digest : request->digests()) {
        bytes_requested += digest.size_bytes();
    }

    if (bytes_requested > response_size_limit) {
        return grpc::Status(
            grpc::StatusCode::INVALID_ARGUMENT,
            "Sum of bytes requested exceeds the gRPC message size limit: " +
                std::to_string(bytes_requested) + " > " +
                std::to_string(response_size_limit) + " bytes");
    }

    CALL_IF_INSTANCE_NAME_MATCHES(d_cas->BatchReadBlobs(*request, response));
}

Status
CasRemoteExecutionServicer::GetTree(ServerContext *,
                                    const GetTreeRequest *request,
                                    ServerWriter<GetTreeResponse> *stream)
{
    CALL_IF_INSTANCE_NAME_MATCHES(d_cas->GetTree(*request, stream));
}

CasBytestreamServicer::CasBytestreamServicer(std::shared_ptr<CasInstance> cas)
    : d_cas(cas)
{
}

Status CasBytestreamServicer::Read(ServerContext *, const ReadRequest *request,
                                   ServerWriter<ReadResponse> *writer)
{
    return d_cas->Read(*request, writer);
}

Status CasBytestreamServicer::Write(ServerContext *,
                                    ServerReader<WriteRequest> *request,
                                    WriteResponse *response)
{
    return d_cas->Write(*request, response);
}

CapabilitiesServicer::CapabilitiesServicer(const std::string &instance_name)
    : d_instance_name(instance_name)
{
    CacheCapabilities *const cache_capabilities =
        d_server_capabilities.mutable_cache_capabilities();

    cache_capabilities->add_digest_function(CASHash::digestFunction());

    cache_capabilities->set_symlink_absolute_path_strategy(
        SymlinkAbsolutePathStrategy_Value_ALLOWED);

    // Since we are running locally, we do not limit the size of
    // batches:
    cache_capabilities->set_max_batch_total_size_bytes(0);

    ActionCacheUpdateCapabilities *const action_cache_update_capabilities =
        cache_capabilities->mutable_action_cache_update_capabilities();
    action_cache_update_capabilities->set_update_enabled(false);
}

Status
CapabilitiesServicer::GetCapabilities(grpc::ServerContext *,
                                      const GetCapabilitiesRequest *request,
                                      ServerCapabilities *response)
{
    if (request->instance_name() == d_instance_name) {
        response->CopyFrom(d_server_capabilities);
        return grpc::Status::OK;
    }

    return INVALID_INSTANCE_NAME_ERROR(request);
}

} // namespace buildboxcasd
