/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_filestager.h>

#include <buildboxcommon_fileutils.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_protos.h>

#include <dirent.h>
#include <fcntl.h>
#include <limits.h>
#include <stdlib.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

using namespace buildboxcasd;
using namespace build::bazel::remote::execution::v2;

FileStager::FileStager(LocalCas *cas_storage) : d_cas_storage(cas_storage) {}

void FileStager::stage(const Digest &root_digest, const std::string &path)
{
    const bool path_is_existing_directory =
        buildboxcommon::FileUtils::is_directory(path.c_str());

    if (path_is_existing_directory &&
        !buildboxcommon::FileUtils::directory_is_empty(path.c_str())) {
        throw std::invalid_argument("Could not stage " +
                                    toString(root_digest) + " as [" + path +
                                    "] is not empty.");
    }
    else {
        buildboxcommon::FileUtils::create_directory(path.c_str());
    }

    try {
        stageDirectory(root_digest, path);
    }
    catch (const std::exception &e) {
        BUILDBOX_LOG_DEBUG("Error staging root digest " << root_digest
                                                        << " in [" << path
                                                        << "]: " << e.what());

        // If the staging fails, we roll back the stage directory to the
        // its status before the call:
        try {
            unstage(path, !path_is_existing_directory);
        }
        catch (const std::system_error &) {
            ;
        }

        throw;
    }
}

void FileStager::unstage(const std::string &path, bool delete_root)
{
    try {
        if (delete_root) {
            buildboxcommon::FileUtils::delete_directory(path.c_str());
        }
        else {
            buildboxcommon::FileUtils::clear_directory(path.c_str());
        }
    }
    catch (const std::system_error &e) {
        BUILDBOX_LOG_ERROR("Error unstaging " << path << ": " << e.what());
        throw e;
    }
}

void FileStager::stageDirectory(const Digest &digest, const std::string &path)
{

    Directory directory = getDirectory(digest);

    createDirectory(path);

    for (const FileNode &file : directory.files()) {
        const std::string stage_path = path + "/" + file.name();
        stageFile(file.digest(), stage_path, file.is_executable());
    }

    for (const DirectoryNode &subdirectory : directory.directories()) {
        const std::string subdirectory_path = path + "/" + subdirectory.name();
        stageDirectory(subdirectory.digest(), subdirectory_path);
    }

    for (const SymlinkNode &symlink : directory.symlinks()) {
        const std::string symlink_path = path + "/" + symlink.name();
        stageSymLink(symlink.target(), symlink_path);
    }
}

void FileStager::stageFile(const Digest &digest, const std::string &path,
                           bool is_executable)
{
    const std::string cas_path = d_cas_storage->path(digest);

    hardLink(cas_path.c_str(), path.c_str());
    // (!) TODO: ensure links are read only

    if (is_executable) {
        try {
            buildboxcommon::FileUtils::make_executable(path.c_str());
        }
        catch (const std::runtime_error &e) {
            BUILDBOX_LOG_ERROR("Could not give executable permission to "
                               << path << ": " << std::strerror(errno));
            throw e;
        }
    }
}

void FileStager::stageSymLink(const std::string &target,
                              const std::string &link_path)
{
    if (target.find("//") != std::string::npos ||
        target.find("/./") != std::string::npos) {
        throw std::invalid_argument(
            "Found forbidden `/./` or `//` in symlink target path.");
    }

    const int link_status = symlink(target.c_str(), link_path.c_str());

    if (link_status != 0) {
        BUILDBOX_LOG_ERROR("Could not create symlink "
                           << link_path << " -> " << target << ": "
                           << std::strerror(errno));
        throw std::system_error(errno, std::system_category());
    }
}

void FileStager::hardLink(const std::string &target,
                          const std::string &link_path)
{
    const int link_status = link(target.c_str(), link_path.c_str());
    // (old path, new path)

    if (link_status != 0) {
        BUILDBOX_LOG_ERROR("Could not create hard link "
                           << link_path << " -> " << target << ": "
                           << std::strerror(errno));
        throw std::system_error(errno, std::system_category());
    }
}

void FileStager::createDirectory(const std::string &path)
{
    try {
        buildboxcommon::FileUtils::create_directory(path.c_str());
    }
    catch (const std::system_error &e) {
        BUILDBOX_LOG_ERROR("Could not create directory " << path << ": "
                                                         << e.what());
        throw e;
    }
}

Directory FileStager::getDirectory(const Digest &digest)
{
    const std::string blob_data = d_cas_storage->readBlob(digest);

    Directory directory;
    if (directory.ParseFromString(blob_data)) {
        return directory;
    }

    std::stringstream error_message;
    error_message << "Could not parse Directory from blob with digest: "
                  << digest;

    BUILDBOX_LOG_ERROR(error_message.str());
    throw std::runtime_error(error_message.str());
}
