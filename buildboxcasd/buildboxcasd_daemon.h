/*
 * Copyright 2018 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#ifndef INCLUDED_BUILDBOXCASD_DAEMON
#define INCLUDED_BUILDBOXCASD_DAEMON

#include <string>

#include <buildboxcommon_connectionoptions.h>
#include <buildboxcommon_logging.h>
#include <buildboxcommon_protos.h>

namespace buildboxcasd {

namespace proto {
using namespace build::bazel::remote::execution::v2;
} // namespace proto

class Daemon {
  public:
    /**
     * Connect to the Bots and CAS servers and run jobs until
     * `d_stopAfterJobs` reaches 0.
     */
    void runDaemon();

    buildboxcommon::ConnectionOptions d_cas_server;

    std::string d_instance_name;
    std::string d_local_cache_path;
    std::string d_bind_address;
    int64_t d_quota_high = 0;
    int64_t d_quota_low = 0;
    bool d_protect_session_blobs = false;
    buildboxcommon::LogLevel d_log_level = defaultLogLevel();

    static buildboxcommon::LogLevel defaultLogLevel()
    {
        return buildboxcommon::LogLevel::ERROR;
    }

  private:
};

} // namespace buildboxcasd

#endif
