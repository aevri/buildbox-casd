/*
 * Copyright 2019 Bloomberg Finance LP
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

#include <buildboxcasd_casinstance.h>
#include <buildboxcommon_client.h>
#include <buildboxcommon_logging.h>

using namespace buildboxcasd;

CasInstance::CasInstance(const std::string &instance_name)
    : d_instance_name(instance_name)
{
}
CasInstance::~CasInstance() {}

grpc::Status CasInstance::Read(const ReadRequest &request,
                               ServerWriter<ReadResponse> *writer)
{
    const std::string &resource_name = request.resource_name();

    BUILDBOX_LOG_DEBUG("Bytestream.Read(" << resource_name << ")");

    if (!readInstanceNameMatches(resource_name)) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "Invalid instance name.");
    }

    Digest digest;
    const grpc::Status argument_status =
        bytestreamReadArgumentStatus(request, &digest);

    if (!argument_status.ok()) {
        return argument_status;
    }
    const auto total_read_limit = request.read_limit();
    /* According to the REAPI specification:
     * "The maximum number of `data` bytes the server is allowed to return in
     * the sum of all `ReadResponse` messages. A `read_limit` of zero
     * indicates that there is no limit, and a negative `read_limit` will
     * cause an error."
     */

    const auto blob_size = digest.size_bytes();
    auto read_offset = request.read_offset();

    google::protobuf::int64 bytes_remaining =
        (total_read_limit == 0) ? blob_size - read_offset : total_read_limit;

    do {
        const auto chunk_size =
            std::min(static_cast<size_t>(bytes_remaining),
                     static_cast<size_t>(
                         buildboxcommon::Client::bytestreamChunkSizeBytes()));
        // We use the same limit defined by `buildboxcommon::Client` for the
        // number of bytes that can be transferred inside a single gRPC
        // message.

        std::string data;
        const auto status = readBlob(
            digest, &data, static_cast<size_t>(read_offset), chunk_size);

        if (status.code() != grpc::StatusCode::OK) {
            return grpc::Status(static_cast<grpc::StatusCode>(status.code()),
                                status.message());
        }

        ReadResponse response;
        response.set_data(data);
        writer->Write(response);

        read_offset += data.size();
        bytes_remaining -= data.size();
    } while (bytes_remaining > 0 && read_offset < blob_size);

    return grpc::Status::OK;
}

grpc::Status CasInstance::Write(ServerReader<WriteRequest> &request,
                                WriteResponse *response)
{
    WriteRequest request_message;
    std::string resource_name;
    Digest digest;

    std::string buffer;
    bool commit_write = false;

    while (request.Read(&request_message)) {
        if (resource_name.empty()) {
            // First request. Initializing, checking and saving arguments:
            response->set_committed_size(0);

            resource_name = request_message.resource_name();
            if (!writeInstanceNameMatches(resource_name)) {
                return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                                    "Invalid instance name.");
            }

            try {
                digest = digestFromUploadResourceName(resource_name);
            }
            catch (const std::invalid_argument &) {
                return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                                    "Invalid resource name.");
            }

            BUILDBOX_LOG_DEBUG("Bytestream.Write(" << resource_name << ")");

            buffer.reserve(static_cast<size_t>(digest.size_bytes()));
        }
        else if (request_message.resource_name() != resource_name) {
            return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                                "`resource_name` changed between requests.");
        }
        else if (commit_write) {
            // `finish_write` must be set only in the last request.
            return grpc::Status(
                grpc::StatusCode::INVALID_ARGUMENT,
                "Extra request sent after setting `finish_write`.");
        }

        const auto bytes_written =
            static_cast<google::protobuf::int64>(buffer.size());
        if (request_message.write_offset() != bytes_written) {
            // (Only serial writes are allowed)
            grpc::Status(grpc::StatusCode::OUT_OF_RANGE,
                         "`write_offset` is not valid");
        }

        // All arguments are valid, appending to the buffer:
        buffer.append(request_message.data());
        commit_write = request_message.finish_write();
    } // Finished receiving requests.

    if (!commit_write) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "`finish_write` was not set in the last request.");
    }

    // We received the whole blob, we can write it.
    // Writing an existing blob to local storage and to a remote in proxy
    // mode are no-ops, so we don't need to check whether the digest is
    // present.
    const google::rpc::Status write_status = writeBlob(digest, buffer);

    if (write_status.code() == grpc::StatusCode::OK) {
        response->set_committed_size(digest.size_bytes());
    }

    return grpc::Status(static_cast<grpc::StatusCode>(write_status.code()),
                        write_status.message());
}

bool CasInstance::readInstanceNameMatches(
    const std::string &resource_name) const
{
    return d_instance_name ==
           instanceNameFromResourceName(resource_name, "blobs");
}

bool CasInstance::writeInstanceNameMatches(
    const std::string &resource_name) const
{
    return d_instance_name ==
           instanceNameFromResourceName(resource_name, "uploads");
}

std::string CasInstance::instanceNameFromResourceName(
    const std::string &resource_name, const std::string &expected_root) const
{

    // "expected_root/..."
    if (resource_name.substr(0, expected_root.size()) == expected_root) {
        return ""; // No instance name specified.
    }

    // "{instance_name}/expected_root/..."
    const auto instance_name_end = resource_name.find("/" + expected_root);
    // (According to the RE specification: "[...] the `instance_name`
    // is an identifier, possibly containing multiple path segments [...]".)
    return resource_name.substr(0, instance_name_end);
}

grpc::Status CasInstance::GetTree(const GetTreeRequest &request,
                                  ServerWriter<GetTreeResponse> *writer)
{
    // TODO: Implement pagination:
    // https://gitlab.com/BuildGrid/buildbox/buildbox-casd/issues/12
    GetTreeResponse response;

    const Digest root_digest = request.root_digest();
    BUILDBOX_LOG_DEBUG("GetTree(" << root_digest.hash() << ")");

    try {
        const Directory root_directory = getDirectory(root_digest);

        buildTree(root_directory, response);

        writer->Write(response);
        return grpc::Status::OK;
    }
    catch (const BlobNotFoundException &) {
        return grpc::Status(grpc::StatusCode::NOT_FOUND,
                            "The root digest was not found in the local CAS.");
    }
    catch (const std::runtime_error &e) {
        BUILDBOX_LOG_ERROR("GetTree(): Error building tree: " << e.what());
    }

    return grpc::Status(grpc::StatusCode::INTERNAL,
                        "Could not build the requested tree.");
}

Directory CasInstance::getDirectory(const Digest &digest)
{
    std::string subdirectory_blob;

    const google::rpc::Status read_status =
        readBlob(digest, &subdirectory_blob, 0, 0);

    if (read_status.code() == grpc::StatusCode::OK) {
        Directory d;
        if (d.ParseFromString(subdirectory_blob)) {
            return d;
        }
        else {
            BUILDBOX_LOG_DEBUG(digest.hash() << "/" << digest.size_bytes()
                                             << ": error parsing Directory.");
        }
    }
    throw BlobNotFoundException(
        "Directory blob could not be read from local CAS.");
}

void CasInstance::buildTree(const Directory &directory,
                            GetTreeResponse &response)
{
    // Adding the directory to the response:
    Directory *directory_entry = response.add_directories();
    directory_entry->CopyFrom(directory);

    // And recursively adding all its subdirectories to the response...
    for (const DirectoryNode &subdirectory_node : directory.directories()) {
        // (sub)DirectoryNode -> directory_node (blob) -> Directory proto
        const Digest subdirectory_digest = subdirectory_node.digest();
        try {
            const Directory subdirectory_proto =
                getDirectory(subdirectory_digest);
            buildTree(subdirectory_proto, response);
        }
        catch (const BlobNotFoundException &) {
            ;
        }
        // (According to the RE specification, if some portion of the tree
        // is missing from the CAS, we still have to return what we have. So we
        // just skip missing subdirectories.)
    }
}

LocalCasInstance::LocalCasInstance(std::shared_ptr<LocalCas> storage,
                                   const std::string &instance_name)
    : CasInstance(instance_name), d_storage(storage)
{
}

grpc::Status
LocalCasInstance::FindMissingBlobs(const FindMissingBlobsRequest &request,
                                   FindMissingBlobsResponse *response)
{
    // Returns a list of digests that are *not* in the CAS.
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::FindMissingBlobs request for instance name \""
        << request.instance_name() << "\" for "
        << request.blob_digests().size() << " digest(s)");

    for (const Digest &digest : request.blob_digests()) {
        bool blob_in_cas = false;

        try {
            blob_in_cas = d_storage->hasBlob(digest);
        }
        catch (const std::runtime_error &) {
            BUILDBOX_LOG_ERROR("Could not determine if "
                               << digest.hash() << "is in local CAS.");
        }

        if (!blob_in_cas) {
            Digest *entry = response->add_missing_blob_digests();
            entry->CopyFrom(digest);
        }
    }
    return grpc::Status::OK;
}

grpc::Status
LocalCasInstance::BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                   BatchUpdateBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::BatchUpdateBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.requests().size()
        << " blob(s)");

    for (const auto &blob : request.requests()) {
        const google::rpc::Status status =
            writeToLocalStorage(blob.digest(), blob.data());

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(blob.digest());
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasInstance::BatchReadBlobs(const BatchReadBlobsRequest &request,
                                 BatchReadBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasInstance::BatchReadBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.digests().size()
        << " digest(s)");

    for (const Digest &digest : request.digests()) {
        std::string data;
        const google::rpc::Status status = readFromLocalStorage(digest, &data);

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(digest);
        entry->mutable_status()->CopyFrom(status);
        entry->set_data(data);
    }
    return grpc::Status::OK;
}

/*
 * Helpers to parse resource name URLs and extract digests
 */
Digest
CasInstance::digestFromDownloadResourceName(const std::string &resource_name)
{
    /* Checks that the resource name has the form:
     * "{instance_name}/blobs/{hash}/{size}",
     * and attempts to parse the hash and size and return a Digest object.
     * If not, raise an invalid argument exception.
     */
    static const std::regex regex("^(?:\\S+/)?blobs/(.+)/(\\d+)");
    return digestFromResourceName(resource_name, regex);
}

Digest
CasInstance::digestFromUploadResourceName(const std::string &resource_name)
{
    /* Checks that the resource name has the form:
     * "{instance_name}/uploads/{uuid}/blobs/{hash}/{size}",
     * and attempts to parse the hash and size and return a Digest object.
     * If not, raise an invalid argument exception.
     */
    static const std::regex regex("^(?:\\S+/)?uploads/.+/blobs/(.+)/(\\d+)");
    return digestFromResourceName(resource_name, regex);
}

Digest CasInstance::digestFromResourceName(const std::string &resource_name,
                                           const std::regex &regex)
{
    std::smatch matches;

    if (std::regex_search(resource_name, matches, regex) &&
        matches.size() == 3) {
        const std::string hash = matches[1];
        const std::string size = matches[2];

        Digest d;
        d.set_hash(hash);
        d.set_size_bytes(std::stoi(size));
        return d;
    }

    throw std::invalid_argument("Resource name \"" + resource_name + "\" " +
                                "is not valid");
}

LocalCasProxyInstance::LocalCasProxyInstance(
    std::shared_ptr<LocalCas> storage,
    std::shared_ptr<buildboxcommon::Client> cas_client,
    const std::string &instance_name)
    : LocalCasInstance(storage, instance_name), d_cas_client(cas_client)
{
}

grpc::Status
LocalCasProxyInstance::FindMissingBlobs(const FindMissingBlobsRequest &request,
                                        FindMissingBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::FindMissingBlobs request for instance name \""
        << request.instance_name() << "\" for "
        << request.blob_digests().size() << " digest(s)");

    const std::vector<Digest> requested_digests(
        request.blob_digests().cbegin(), request.blob_digests().cend());

    const std::vector<Digest> digests_missing_in_remote =
        d_cas_client->findMissingBlobs(requested_digests);

    for (const Digest &digest : digests_missing_in_remote) {
        if (d_storage->hasBlob(digest)) {
            // If we have the blob stored locally, we implicitly update the
            // remote.
            try {
                d_cas_client->upload(d_storage->readBlob(digest), digest);
                // If the upload succeeded we know that the blob is now in
                // the remote. We can skip adding it to the returned blob
                // list.
                continue;
            }
            catch (const std::runtime_error &e) {
                BUILDBOX_LOG_ERROR("Error uploading " << toString(digest)
                                                      << ": " << e.what());
            }
        }

        Digest *entry = response->add_missing_blob_digests();
        entry->CopyFrom(digest);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasProxyInstance::BatchUpdateBlobs(const BatchUpdateBlobsRequest &request,
                                        BatchUpdateBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::BatchUpdateBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.requests().size()
        << " blob(s)");

    // TODO: Issue a FindMissingBlobs request first to avoid sending
    // duplicates.
    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    for (const auto &blob : request.requests()) {
        buildboxcommon::Client::UploadRequest upload_request(blob.digest(),
                                                             blob.data());
        upload_requests.push_back(upload_request);

        writeToLocalStorage(blob.digest(), blob.data());
    }

    const std::set<std::string> digests_not_uploaded =
        batchUpdateRemoteCas(request);
    for (const auto &blob : request.requests()) {
        google::rpc::Status status;
        if (digests_not_uploaded.count(blob.digest().hash())) {
            status.set_code(grpc::StatusCode::UNAVAILABLE);
            status.set_message("Could not update blob to remote CAS");
        }
        else {
            status.set_code(grpc::StatusCode::OK);
        }

        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(blob.digest());
        entry->mutable_status()->CopyFrom(status);
    }

    return grpc::Status::OK;
}

grpc::Status
LocalCasProxyInstance::BatchReadBlobs(const BatchReadBlobsRequest &request,
                                      BatchReadBlobsResponse *response)
{
    BUILDBOX_LOG_INFO(
        "LocalCasProxyInstance::BatchReadBlobs request for instance name \""
        << request.instance_name() << "\" for " << request.digests().size()
        << " digest(s)");

    // Checking the local storage first:
    std::vector<Digest> digests_missing_locally;
    for (const Digest &digest : request.digests()) {
        std::string data;
        const google::rpc::Status read_status =
            readFromLocalStorage(digest, &data);

        if (read_status.code() == grpc::StatusCode::OK) {
            auto entry = response->add_responses();
            entry->mutable_digest()->CopyFrom(digest);
            entry->mutable_status()->CopyFrom(read_status);
            entry->set_data(data);
        }
        else {
            digests_missing_locally.push_back(digest);
        }
    }

    if (digests_missing_locally.empty()) {
        BUILDBOX_LOG_INFO("retrieved all digest(s) for instance name \""
                          << request.instance_name()
                          << "\" from local storage");
        return grpc::Status::OK;
    }

    // Making a request for the blobs that we couldn't find locally:
    const buildboxcommon::Client::DownloadedData downloaded_data =
        d_cas_client->downloadBlobs(digests_missing_locally);

    size_t count = 0;
    for (const Digest &digest : digests_missing_locally) {
        auto entry = response->add_responses();
        entry->mutable_digest()->CopyFrom(digest);

        google::rpc::Status status;

        const auto digest_data_it = downloaded_data.find(digest.hash());
        if (digest_data_it != downloaded_data.cend()) {
            const std::string data = digest_data_it->second;
            status.set_code(grpc::StatusCode::OK);
            entry->set_data(data);

            writeToLocalStorage(digest, data);
            ++count;
        }
        else {
            status.set_code(grpc::StatusCode::NOT_FOUND);
            status.set_message("Blob could not be fetched from remote CAS");
        }

        entry->mutable_status()->CopyFrom(status);
    }

    BUILDBOX_LOG_INFO("downloaded "
                      << count << " out of " << digests_missing_locally.size()
                      << " digest(s) from the remote cas server");

    return grpc::Status::OK;
}

bool LocalCasProxyInstance::hasBlob(const Digest &digest)
{
    try {
        if (!d_storage->hasBlob(digest)) {
            const std::string directory_entry =
                d_cas_client->fetchString(digest);
            d_storage->writeBlob(digest, directory_entry);
        }
        return true;
    }
    catch (const std::runtime_error &) {
        return false;
    }
}

google::rpc::Status LocalCasProxyInstance::readBlob(const Digest &digest,
                                                    std::string *data,
                                                    size_t read_offset,
                                                    size_t read_limit)
{
    const google::rpc::Status read_status =
        readFromLocalStorage(digest, data, read_offset, read_limit);
    if (read_status.code() == grpc::StatusCode::OK) {
        return read_status;
    }

    google::rpc::Status status;
    try {
        const std::string fetched_data = d_cas_client->fetchString(digest);

        if (read_limit == 0) {
            *data = fetched_data.substr(read_offset);
        }
        else {
            *data = fetched_data.substr(read_offset, read_limit);
        }

        status.set_code(grpc::StatusCode::OK);

        writeToLocalStorage(digest, fetched_data);
    }
    catch (const std::runtime_error &) {
        status.set_code(grpc::StatusCode::NOT_FOUND);
        status.set_message("Blob not found in local nor remote CAS");
    }

    return status;
}

google::rpc::Status LocalCasProxyInstance::writeBlob(const Digest &digest,
                                                     const std::string &data)
{
    BUILDBOX_LOG_INFO("LocalCasProxyInstance::writeBlob: writing digest of "
                      << digest.size_bytes() << " bytes");

    google::rpc::Status status;

    // Trying to upload the blob first:
    try {
        d_cas_client->upload(data, digest);
    }
    catch (const std::logic_error &) { // Digest does not match the data.
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        return status;
    }
    catch (const std::runtime_error &) {
        status.set_code(grpc::StatusCode::UNAVAILABLE);
        return status;
    }

    // Upload was successful. We now write to the local storage
    // (if needed, LocalCas will skip the write if the file exists.):
    google::rpc::Status write_status = writeToLocalStorage(digest, data);
    status.set_code(write_status.code());

    return status;
}

std::set<std::string> LocalCasProxyInstance::batchUpdateRemoteCas(
    const BatchUpdateBlobsRequest &request)
{
    std::vector<buildboxcommon::Client::UploadRequest> upload_requests;
    for (const auto &blob : request.requests()) {
        buildboxcommon::Client::UploadRequest upload_request(blob.digest(),
                                                             blob.data());
        upload_requests.push_back(upload_request);
    }

    const std::vector<buildboxcommon::Client::UploadResult>
        not_uploaded_blobs = d_cas_client->uploadBlobs(upload_requests);

    std::set<std::string> res;
    for (const buildboxcommon::Client::UploadResult &upload_result :
         not_uploaded_blobs) {
        res.insert(upload_result.digest.hash());
    }

    return res;
}

google::rpc::Status
LocalCasInstance::writeToLocalStorage(const Digest &digest,
                                      const std::string data)
{
    google::rpc::Status status;

    try {
        d_storage->writeBlob(digest, data);
        status.set_code(grpc::StatusCode::OK);
    }
    catch (const std::invalid_argument &) {
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        status.set_message(
            "The size of the data does not match the size defined "
            "in the digest.");
    }
    catch (const std::runtime_error &e) {
        status.set_code(grpc::StatusCode::INTERNAL);
        status.set_message("Internal error while writing blob to local CAS: " +
                           std::string(e.what()));
    }

    return status;
}

google::rpc::Status
LocalCasInstance::readFromLocalStorage(const Digest &digest,
                                       std::string *data) const
{
    return readFromLocalStorage(digest, data, 0, 0);
}

google::rpc::Status
LocalCasInstance::readFromLocalStorage(const Digest &digest, std::string *data,
                                       size_t offset, size_t limit) const
{
    google::rpc::Status status;

    try {
        const size_t read_length = (limit > 0) ? limit : LocalCas::npos;
        *data = d_storage->readBlob(digest, offset, read_length);
        status.set_code(grpc::StatusCode::OK);
    }
    catch (const BlobNotFoundException &) {
        status.set_code(grpc::StatusCode::NOT_FOUND);
        status.set_message("Blob not found in the local CAS.");
    }
    catch (const std::out_of_range &) {
        status.set_code(grpc::StatusCode::INVALID_ARGUMENT);
        status.set_message("Read interval is out of range.");
    }
    catch (const std::runtime_error &e) {
        status.set_code(grpc::StatusCode::INTERNAL);
        status.set_message(
            "Internal error while fetching blob in local CAS: " +
            std::string(e.what()));
    }

    return status;
}

bool LocalCasInstance::hasBlob(const Digest &digest)
{
    return d_storage->hasBlob(digest);
}

google::rpc::Status LocalCasInstance::readBlob(const Digest &digest,
                                               std::string *data,
                                               size_t read_offset,
                                               size_t read_limit)
{
    return readFromLocalStorage(digest, data, read_offset, read_limit);
}

google::rpc::Status LocalCasInstance::writeBlob(const Digest &digest,
                                                const std::string &data)
{
    BUILDBOX_LOG_INFO("LocalCasInstance::writeBlob: writing digest of "
                      << digest.size_bytes() << " bytes");
    return writeToLocalStorage(digest, data);
}

grpc::Status
CasInstance::bytestreamReadArgumentStatus(const ReadRequest &request,
                                          Digest *digest)
{
    const auto read_limit = request.read_limit();
    if (read_limit < 0) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "`read_limit` cannot be negative.");
    }

    const auto read_offset = request.read_offset();
    if (read_offset < 0) {
        return grpc::Status(grpc::StatusCode::OUT_OF_RANGE,
                            "`read_offset` cannot be negative.");
    }

    // Checking that the resource name of the request is pertinent and if
    // so extracting the Digest value from it:
    const std::string resource_name = request.resource_name();
    try {
        const Digest requested_digest =
            digestFromDownloadResourceName(resource_name);

        if (read_offset > requested_digest.size_bytes()) {
            return grpc::Status(
                grpc::StatusCode::OUT_OF_RANGE,
                "`read_offset` cannot be larger than the size of the "
                "data: " +
                    std::to_string(read_offset) + " > " +
                    std::to_string(requested_digest.size_bytes()));
        }

        digest->CopyFrom(requested_digest);
        return grpc::Status::OK;
    }
    catch (const std::invalid_argument &) {
        return grpc::Status(grpc::StatusCode::INVALID_ARGUMENT,
                            "The requested resource (\"" + resource_name +
                                "\") is not valid.");
    }
}
