image: registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest

stages:
  - build
  - test
  - release

build:
  stage: build
  script:
    - mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=DEBUG .. && make -j$(nproc) || exit 1

# Check C++ code formatting using clang-format
# Since the GitLab CI API doesn't currently provide an MR Commit SHA so that we can
# run all the files modified in the current MR (Single Commit is doable) we just
# run clang-format for the diff between "empty tree magic SHA" and the current commit
# on all the C++ files (by extension: c,cc,cpp,cxx,h)
# Discussion on the "empty tree magic SHA": https://stackoverflow.com/questions/9765453/
check_formatting:
    image: ubuntu:bionic
    stage: build
    before_script:
        - apt-get update && apt-get install -y clang-format-6.0 git-core
    script:
        - echo `which clang-format-6.0`
        - ln -s `which clang-format-6.0` /usr/bin/clang-format
        - cd "$CI_PROJECT_DIR"
        - export CLANG_FORMAT_SINCE_COMMIT="4b825dc642cb6eb9a060e54bf8d69288fbee4904"
        - linter_errors=$(git-clang-format-6.0 --commit "$CLANG_FORMAT_SINCE_COMMIT" -q --diff --style file --extensions c,cc,cpp,cxx,h | grep -v --color=never "no modified files to format" || true)
        - echo "$linter_errors"
        - if [[ ! -z "$linter_errors" ]]; then echo "Detected formatting issues; please fix"; exit 1; else echo "Formatting is correct"; exit 0; fi

test_cover:
  stage: test
  coverage: '/^TOTAL.*\ (\d+\.*\d+\%)$/'
  dependencies: []
  before_script:
    - apt-get update && apt-get install -y make cmake build-essential pkg-config libssl-dev googletest libcurl4-openssl-dev
    - apt-get install -y python3 python3-pip wget
    - pip3 install gcovr pycobertura
  script:
    - mkdir build
    - cd build
    - export COV_FLGS="-g -O0 -fprofile-arcs -ftest-coverage"
    - cmake -DGTEST_SOURCE_ROOT=/usr/src/googletest -DCMAKE_CXX_FLAGS="${COV_FLGS}" -DCMAKE_EXE_LINKER_FLAGS="${COV_FLGS}" -DCMAKE_SHARED_LINKER_FLAGS="${COV_FLGS}" -DCMAKE_CXX_OUTPUT_EXTENSION_REPLACE="ON" .. || exit 1
    - make -j$(nproc) || exit 1
    - make -j$(nproc) test || exit 1
    - cd $CI_PROJECT_DIR
    - echo "Generating gcov files"
    - mkdir gcov && cd gcov
    - find "$CI_PROJECT_DIR/" -type f -name "*.gcno" -exec gcov --branch-counts --branch-probabilities --preserve-paths {} \;
    # remove gcov files that come from dependencies so that they don't show up on reports
    - find "$CI_PROJECT_DIR/gcov/" -type f ! -name "#*#buildboxcasd#*" -exec rm "{}" \;
    - cd "$CI_PROJECT_DIR"
    - mkdir -p public/coverage
    - export LC_ALL=C.UTF-8 && export LANG=C.UTF-8
    - gcovr --xml-pretty -g -k > public/coverage/gcovr.xml
    - pycobertura show --format text --output public/coverage/report.txt -s "$CI_PROJECT_DIR" public/coverage/gcovr.xml
    - echo >> public/coverage/report.txt
    - pycobertura show --format html --output public/coverage/report.html -s "$CI_PROJECT_DIR" public/coverage/gcovr.xml
    - cat public/coverage/report.txt # show the textual report here, also used by GitLab to pick-up coverage %
  artifacts:
    paths:
      - public # name GitLab uses for Pages feature
      - build/

integration_test:
  stage: test
  dependencies: [build]
  cache:
    key: integration-test-input-dir-key
    paths:
      - build/test_data/
  before_script:
    - apt-get update && apt-get install -y curl
    - mkdir -p build
    - cd build
    - cmake .. && make -j$(nproc) integration_test
    - mkdir -p test_data
    - cd test_data
    - curl "https://mirror.us-midwest-1.nexcess.net/gnu/hello/hello-2.1.0.tar.gz" | tar xz
    - dd if=/dev/zero of=zeroes.blob bs=1M count=16
    - dd if=/dev/urandom of=16M.blob bs=1M count=16
    - dd if=/dev/urandom of=64M.blob bs=1M count=64
    - dd if=/dev/urandom of=256M.blob bs=1M count=256
    - cd ..
  script:
    - INPUT_DIRECTORY="test_data/"
    - SOCKETS_PATH=$(mktemp -d)
    - PROXY_ENDPOINT="unix:$SOCKETS_PATH/proxy.sock"
    - SERVER_ENDPOINT="unix:$SOCKETS_PATH/server.sock"
    - CACHES_DIR=$(mktemp -d)
    - DOWNLOADS_DIR=$(mktemp -d)
    # Launching server:
    - nohup bash -c "./buildboxcasd/buildbox-casd --bind="$SERVER_ENDPOINT"
      "$CACHES_DIR/server" 2>&1" &
    # Launching proxy:
    - nohup bash -c "./buildboxcasd/buildbox-casd --bind="$PROXY_ENDPOINT"
      --cas-remote="$SERVER_ENDPOINT" "$CACHES_DIR/proxy" 2>&1" &
    - ./test/integration_test "$PROXY_ENDPOINT" "$SERVER_ENDPOINT" "$INPUT_DIRECTORY" "$DOWNLOADS_DIR" || exit 1
    # Comparing contents:
    - (diff -r "$INPUT_DIRECTORY" "$DOWNLOADS_DIR/proxy-output"
       && diff -r "$INPUT_DIRECTORY" "$DOWNLOADS_DIR/server-output"
      ) || (echo "Directory contents do not match"; exit 1)
    # Checking permissions:
    - |
      function executable_files() {
        find "$1" -executable -printf "%P\n";  # (Print paths relative to $1)
      }
    - INPUT_DIR_EXECUTABLES=$(executable_files $INPUT_DIRECTORY)
    - (diff <(echo "$INPUT_DIR_EXECUTABLES") <(executable_files "$DOWNLOADS_DIR/proxy-output")
       && diff <(echo "$INPUT_DIR_EXECUTABLES") <(executable_files "$DOWNLOADS_DIR/server-output")
      ) || (echo "Executable permissions do not match"; exit 1)

e2e:
  image: registry.gitlab.com/buildgrid/buildbox/buildbox-e2e:latest
  stage: test
  dependencies: []
  script:
    - BUILDBOX_CASD_SOURCE_ROOT=`pwd` end-to-end-test.sh

trigger_e2e_rebuild:
  image: ubuntu:bionic
  stage: release
  variables:
    GIT_STRATEGY: none
  script:
    - apt-get update && apt-get install -y curl
    - curl --request POST --form "token=$CI_JOB_TOKEN" --form ref=master https://gitlab.com/api/v4/projects/buildgrid%2Fbuildbox%2Fbuildbox-e2e/trigger/pipeline
  only:
    - master
