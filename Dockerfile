FROM registry.gitlab.com/buildgrid/buildbox/buildbox-common:latest
COPY . /casd
RUN cd /casd && mkdir build && cd build && cmake -DCMAKE_BUILD_TYPE=DEBUG .. && make install && ctest --verbose
