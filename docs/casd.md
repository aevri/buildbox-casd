## Modes of operation
casd can be launched in one of two modes:

* As a local **CAS server**, or

* as a **CAS proxy/cache** pointing to a remote CAS server.

In addition to the Remote Execution API CAS service calls, casd also provides the [LocalCAS protocol](https://gitlab.com/BuildGrid/buildbox/buildbox-common/blob/master/protos/build/buildgrid/local_cas.proto) methods.

### Server mode
In server mode casd works like a regular CAS implementation.

### Proxy-cache mode
When set up in proxy mode, casd is pointed to a remote CAS server and will seek to minimize the data that needs to be transferred to and from that remote by caching data locally.

(In the future, casd will support pointing to multiple remote CAS servers.)

#### Call behaviors
##### `FindMissingBlobs()`
The request is forwarded as-is to the remote CAS.

For each digest reported missing by the remote, checks whether a copy of that blob is stored locally and in that case attempts to upload it to the remote. If the update is successful that digest is not included in the return list.

The returned list is guaranteed to contain the subset of digests that are still missing in the remote.

##### `BatchReadBlobs()`
Only fetches from the remote digests that are not present locally and adds them to the storage (so subsequent calls will return the local copy).

##### `BatchUpdateBlobs()`
Forwards the request as-is to the remote and saves a local copy of each blob.

**Note: this can be optimized by issuing a `FindMissingBlobs()` call before uploading.**

##### `GetTree()`
This call follows the logic for the ByteStream `Read()` and `Write()` for each blob in the tree.

**Note:** Pagination is currently [not supported](https://gitlab.com/BuildGrid/buildbox/buildbox-casd/issues/12).

##### Bytestream
###### `Read()`
If the data is present locally it is served from the local copy. Otherwise forwards the request to the remote and, if successful, adds the result to the local storage.

###### `Write()`
Forwards the request to the remote. If the upload succeeds and the data is not already present, adds a copy of the blob to the local storage.

### LocalCAS protocol
casd implements the [LocalCAS protocol](https://gitlab.com/BuildGrid/buildbox/buildbox-common/blob/master/protos/build/buildgrid/local_cas.proto) methods.

#### `StageTree()`
This method makes the specified directory tree temporarily available for local filesystem access.

Currently casd implements this operation employing read-only hard links to the actual blobs stored in `CAS_PATH`, which is simple and avoids doing copies. However, a limitation of relying on hard links is that it only supports staging into directories inside the same filesystem that hosts the `CAS_PATH` directory.

There are plans to support other staging mechanisms such as [FUSE](https://gitlab.com/BuildGrid/buildbox/buildbox-casd/issues/8).

# Storage
All blobs are stored on disk in the specified `CAS_PATH` directory that will have this structure:
* `CAS_PATH/cas/objects`: contains subdirectories named after the first two most-significant hex digits of the digest. The blobs are stored in those directories in files named after the rest of the digits.

* `CAS_PATH/cas/tmp`: that directory is used to create temporary files used during writes (see the *Concurrency* section)

For example, the blob for digest `e3b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855` will be in `CAS_PATH/cas/objects/e3/b0c44298fc1c149afbf4c8996fb92427ae41e4649b934ca495991b7852b855`

**Note:** External processes are allowed to access blobs directly from the `CAS_PATH` directory. However, that is limited to **read-only** operations.

## Concurrency
casd does not use any locking mechanism for reads and writes. To guarantee concurrency, writes are performed using an intermediate temporary file:

1. Create a temporary file using `mkdtemp()` (which is an atomic operation)
2. Write the contents to it (the current process has exclusive ownership of it)
3. Create a hard link from the final destination to the temporary file (also an atomic operation)
4. `unlink()` the temporary file

This works because of the guarantee that—because we write to files with their digests as names—the contents to be written to a file are going to be the same across all processes.

Because hard links can only link two entries in the same filesystem, intermediate files are created under `CAS_PATH/tmp`.

## Expiry
In order to limit the amount of disk space used, casd provides an [expiry mechanism](https://gitlab.com/BuildGrid/buildbox/buildbox-casd/issues/6) based on the LRU (*Least Recently Used*) algorithm.

The mechanism (disabled by default) can be configured with two values: a low-watermark and a high-watermark. When the disk usage exceeds the value of the high-watermark, the cleanup process is triggered. Data is then deleted, starting from the oldest entries, until the size in use falls under the low-watermark.

That mechanism relies only on the `mtime` value provided by the filesystem to determine the age of an entry and it is *blob-based*: deletes blobs with the oldest timestamps without taking references into account.

(The amount of storage in use is tracked internally by a variable of type `std::atomic<int64_t>`, which is suitable since casd is the only process that can write files into the storage.)
